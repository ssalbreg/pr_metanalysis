

############## PLOT CURVES FOR R & NPP FROM THE SAME STUDY AND SPECIES ###
library('RColorBrewer')    
library("TeachingDemos")
source("/Users/soflysb/ImperialCollege/PR_metanalysis/code/Plots/WeightedAveE.R")
source("/Users/soflysb/ImperialCollege/PR_metanalysis/code/Plots/LabelColors.R")


pdf(file="/Users/soflysb/ImperialCollege/PR_metanalysis/results/Plots/IntraspPlotsTerAq.pdf",width=7,height=3.5,family="Times",pointsize=10)

par(mar = c(0,0,0.5,0.5) + 0.1,
          mgp=c(2.5,0.25,0),tcl=0.2)

split.screen(matrix(c(0,   1/2, 0, 1,
                      1/2, 1, 0, 1), ncol=4, byrow=TRUE))

par(oma = c(3,4,0,2.5) + 0.1)

ylims <- c(-2,5)
ticks <- seq(from=round(-2), to=round(5), length=3)
lab <- format(ticks,digits=2)


##### CASE 1

source("/Users/soflysb/ImperialCollege/Ecoinformatics/Analysis/code/TPCFitting.R")
assign("Tref", 283.15, envir = .GlobalEnv)

tmp_temps <- seq(5, 60, by = 0.5)+273.15
dan_NP = log(exp(Schoolfield(lnB0=2, E=0.56, E_D=4, T_h=310.15,temp=tmp_temps)))
dan_NP2 = log(exp(Schoolfield(lnB0=2, E=0.34, E_D=4, T_h=310.15,temp=tmp_temps)))
dan_R = log(exp(Schoolfield(lnB0=0.2, E=0.56, E_D=4, T_h=330.15, temp=tmp_temps)))
Which <- which.max(dan_NP)


screen(1)
plot(tmp_temps- 273.15, dan_NP, type = "n", axes = FALSE,xlim=c(0,60)) ## no axes
lim <- par("usr")
rect(tmp_temps[Which]-273.15, lim[3]-1, lim[2], lim[4]+1, border = "gray95", col = "gray95")

par(new=TRUE)
plot(tmp_temps- 273.15, dan_NP, type='l', lwd=2, col=BlueNP, 
     ylim=ylims, ylab="",yaxt="n", 
     xlab="",xlim=c(0,60),lty=3)
axis(side=2, at=ticks, labels=lab)
mtext(2,text=expression("("*italic(mu)*"mol CO"[2]*" m"^{-2}*" s"^{-1}*")"),line=1.5)
mtext(2,text="log(metabolic rate)",line=2.7)

m_t1 = tmp_temps[which.max(dan_NP)] - 273.15
segments(x0=m_t1, x1=m_t1, y0=-5, y1=6, col="black",lty=5)

lines(tmp_temps - 273.15, dan_R, lwd=2, col=RedResp,lty=3)
lines(tmp_temps[1:Which] - 273.15, dan_R[1:Which], lwd=2, col=RedResp)
lines((tmp_temps[1:Which] - 273.15), dan_NP[1:Which], lwd=2, col=BlueNP)

lines(tmp_temps[1:Which] - 273.15, dan_NP2[1:Which], lwd=2, col=BlueNP,lty=5)
#lines(tmp_temps[Which:length(dan_NP2)] - 273.15, dan_NP2[Which:length(dan_NP2)], lwd=2, col=BlueNP,lty=3)


m_t = 10
segments(x0=m_t, x1=m_t, y0=-20, y1=dan_NP[which(tmp_temps==273.15+10)], col=adjustcolor('black', alpha.f=0.3))
segments(x0=-10, x1=10, y0=dan_NP[which(tmp_temps==273.15+10)], y1=dan_NP[which(tmp_temps==273.15+10)], col=adjustcolor(BlueNP, alpha.f=0.3))
segments(x0=-10, x1=10, y0=dan_R[which(tmp_temps==273.15+10)], y1=dan_R[which(tmp_temps==273.15+10)], col=adjustcolor(RedResp, alpha.f=0.3))

mtext(expression(italic("P")['0']), side = 2, at = dan_NP[which(tmp_temps==273.15+8)], line = -1, cex = 0.75,las=1)
mtext(expression(italic("R")['0']), side = 2, at = dan_R[which(tmp_temps==273.15+8)], line = -1, cex = 0.75,las=1)
          

text(x = 33, y = dan_R[which(tmp_temps==(273.15+18))], expression(italic(E)[italic(R)]))
arrows(x0=33, y0=dan_R[which(tmp_temps==(273.15+21))], x1 = 30, y1 =dan_R[which(tmp_temps==(273.15+30))], length = 0.08,col="black",lty=1)
text(x = 21, y = dan_NP[which(tmp_temps==(273.15+36))], expression(italic(E)[italic(P)]))
arrows(x0=23, y0=dan_NP[which(tmp_temps==(273.15+40))], x1 = 30, y1 =dan_NP[which(tmp_temps==(273.15+30))], length = 0.08,col="black",lty=1)

mtext(expression(italic("T")['pk']), side = 1, at = m_t1 - 2.6, line = -1)
mtext(expression(italic("T")['ref']), side = 1, at = m_t - 2.6, line = -1)

arrows(x0=m_t, y0= dan_R[which(tmp_temps==(273.15+10))], x1 = m_t, y1 = dan_NP[which(tmp_temps==(273.15+10))], length = 0.08, angle = 30,code = 3,col="black",lty=1)
text(x = 11, y =(dan_R[which(tmp_temps==(273.15+8))]+dan_NP[which(tmp_temps==(273.15+10))])/2, expression(italic("c")))

arrows(x0=-2, y0=-1.5, x1 = m_t1, y1 =-1.5, length = 0.1, angle = 30,code = 3,col="black",lty=1)
text(x = 19, y =-1.25, expression("OTR"))

Evalue <- c(paste(),expression(),substitute(paste(italic(E)[italic(R)],"= 0.56")))
text(x = -1, y =4, Evalue,pos=4)
Evalue <- c(paste(),expression(),substitute(paste(italic(E)[italic(P)],"= 0.56")))
text(x = -1, y =3.6, Evalue,pos=4)

addlabel(0.02,0.07,"A")


## NetFlux

NetFlux <-log((exp(dan_NP)*0.5-exp(dan_R)*0.5))
NetFlux2 <-log((exp(dan_NP2)*0.5-exp(dan_R)*0.5))
par(new=TRUE)
plot(tmp_temps[1:Which]-273.15,NetFlux[1:Which],type="l",ylab="",xlab="",lty=1,xaxt="n",yaxt="n",xlim=c(0,60),ylim=ylims,lwd=2)
#lines(tmp_temps[Which:length(NetFlux)] - 273.15, NetFlux[Which:length(NetFlux)], lwd=2, lty=3)
lines(tmp_temps[1:Which]-273.15,NetFlux2[1:Which],lwd=1.5,lty=5)
#lines(tmp_temps[Which:length(NetFlux)] - 273.15, NetFlux2[Which:length(NetFlux)], lwd=2, lty=3)
par(new=FALSE)
mtext(1,text=expression("Temperature ("^{o}*C*")"),line=1.6)

par(new=FALSE)


#### EXAMPLE EMPIRICAL DATA
source("/Users/soflysb/ImperialCollege/Ecoinformatics/Analysis/code/TPCFitting.R")

TerTPC <-"Cenchrus ciliaris_Ludlow, M., & Wilson, G. (1971). Photosynthesis of tropical pasture plants I. Illuminance, carbon dioxide concentration, leaf temperature, and leaf-air vapour pressure difference. Australian Journal of Biological Sciences, 449_470. Retrieved from http://www.publish.csiro.au/?paper=BI9710449"

WHICH <- which(!is.na(match(paste(TerData$ConSpecies,TerData$Citation,sep="_"),TerTPC)))
Resp <- WHICH[which(TerData$StandardisedTraitName[WHICH]=="respiration rate")]
NPP <- WHICH[which(TerData$StandardisedTraitName[WHICH]=="net photosynthesis")]
ResR <- TResults[which(!is.na(match(TResults$id,unique(TerData$FinalID[Resp])))),]
ResNPP <- TResults[which(!is.na(match(TResults$id,unique(TerData$FinalID[NPP])))),]

# Generate predictions from the model fit...
## RESP
tmp_temps <- seq(min(
    floor(TerData$ConTemp[Resp]+273.15)), 
                 ceiling(max(TerData$ConTemp[Resp]+273.15)
                         ), length = 200)

tmp_model <- exp(Schoolfield(
    ResR$lnB0_sch,
    ResR$E_sch,
    ResR$E_D_sch,
    ResR$T_h_sch,
    tmp_temps
    ))


ModelToPlotRes <- data.frame(
    Temperature = tmp_temps - 273.15, 
    TraitValue = log(tmp_model*1000000)
    )

# Prepare the data points of the original values.
DataToPlotRes <- data.frame(
    Temperature = TerData$ConTemp[Resp], 
    TraitValue = log(TerData$StandardisedTraitValue[Resp]*1000000)
    )
DataToPlotRes <- na.omit(DataToPlotRes)

## PHOTO

tmp_temps <- seq(min(
    floor(TerData$ConTemp[NPP]+273.15)), 
                 ceiling(max(TerData$ConTemp[NPP]+273.15)
                         ), length = 200)

tmp_model <- exp(Schoolfield(
    ResNPP$lnB0_sch,
    ResNPP$E_sch,
    ResNPP$E_D_sch,
    ResNPP$T_h_sch,
    tmp_temps
    ))


ModelToPlotNP <- data.frame(
    Temperature = tmp_temps - 273.15, 
    TraitValue = log(tmp_model*1000000)
    )

# Prepare the data points of the original values.
DataToPlotNP <- data.frame(
    Temperature = TerData$ConTemp[NPP], 
    TraitValue = log(TerData$StandardisedTraitValue[NPP]*1000000)
    )
DataToPlotNP <- na.omit(DataToPlotNP)


Which <- which.max(ModelToPlotNP$TraitValue)

### PLOT
screen(2)
plot(DataToPlotRes$TraitValue~DataToPlotRes$Temperature, type = "n", axes = FALSE,xlim=c(0,60)) ## no axes
lim <- par("usr")
rect(tmp_temps[Which]-273.15, lim[3]-1, lim[2], lim[4]+1, border = "gray95", col = "gray95")

par(new=TRUE)
plot(DataToPlotRes$TraitValue~DataToPlotRes$Temperature,ylab="",pch=19,ylim=c(ylims),yaxt="n",col=RedResp,xlab="",xlim=c(0,60),cex=0.55)


Evalue <- c(paste(),expression(),substitute(paste(italic(E)[italic(R)],"=",y,""),list(y=formatC(ResR$E_sch, digits = 2,format = "f"))),substitute(paste(italic(E)[italic(P)],"=",y,""),list(y=formatC(ResNPP$E_sch, digits = 2,format = "f"))),expression(italic("Cenchrus ciliaris")))

WhichRData <- which.min(abs(DataToPlotRes$Temperature-ModelToPlotNP$Temperature[Which]))
WhichR <- which.min(abs(ModelToPlotRes$Temperature-ModelToPlotNP$Temperature[Which]))
points(DataToPlotRes$Temperature[1:WhichRData],DataToPlotRes$TraitValue[1:WhichRData],col=RedResp,cex=0.75,pch=19)
lines(ModelToPlotRes$Temperature,ModelToPlotRes$TraitValue,col=RedResp,lwd=2,lty=3)
lines(ModelToPlotRes$Temperature[1:WhichR],ModelToPlotRes$TraitValue[1:WhichR],col=RedResp,lwd=2)
addlabel(0.02,0.07,"B")
legend("bottomleft",Evalue,bty="n",inset=-0.02)    

WhichPData <- which.min(abs(DataToPlotNP$Temperature-ModelToPlotNP$Temperature[Which]))
points(DataToPlotNP$TraitValue~DataToPlotNP$Temperature,pch=19,cex=0.55,ylim=ylims,col=BlueNP)
lines(ModelToPlotNP$Temperature,ModelToPlotNP$TraitValue,col=BlueNP,lwd=2,lty=3)    
lines(ModelToPlotNP$Temperature[1:Which],ModelToPlotNP$TraitValue[1:Which],col=BlueNP,lwd=2)    
m_t1 = tmp_temps[which.max(ModelToPlotNP$TraitValue)] - 273.15
segments(x0=m_t1, x1=m_t1, y0=-28, y1=-7, col="black",lty=5)


## ## INSET

MIN <- min(c(exp(ModelToPlotNP$TraitValue),exp(ModelToPlotRes$TraitValue)))
MAX <- max(c(exp(ModelToPlotNP$TraitValue),exp(ModelToPlotRes$TraitValue)))

ticks1 <- seq(from=10, to=50, length=3)
lab1 <- format(ticks1,digits=2)
ticks2 <- seq(0, 60, length=3)
lab2 <- format(ticks2,digits=2)


x <- function() {
 
    Which <- which.max(exp(ModelToPlotNP$TraitValue))
    plot(ModelToPlotNP$Temperature,exp(ModelToPlotNP$TraitValue),col=BlueNP,lwd=2,type="l", xlab='', ylab='', xaxt="n",yaxt="n", xlim=c(0,60),ylim=c(MIN,MAX),lty=2);
    rect(par("usr")[1],par("usr")[3],par("usr")[2],par("usr")[4],col = "white");
    lines(ModelToPlotNP$Temperature,exp(ModelToPlotNP$TraitValue),col=BlueNP,lwd=2,lty=3);
    lines(ModelToPlotNP$Temperature[1:Which],exp(ModelToPlotNP$TraitValue)[1:Which],col=BlueNP,lwd=2,lty=1);
    axis(side=1, at=ticks1, labels=lab1) ;
    axis(side=2, at=ticks2, labels=lab2);
    mtext(2,text="metabolic rate",line=1);
    lines(ModelToPlotRes$Temperature,exp(ModelToPlotRes$TraitValue),col=RedResp,lwd=2,lty=3);
    WhichR <- which.min(abs(ModelToPlotRes$Temperature-ModelToPlotNP$Temperature[Which]))
    lines(ModelToPlotRes$Temperature[1:WhichR],exp(ModelToPlotRes$TraitValue)[1:WhichR],col=RedResp,lwd=2)}

subplot(x(), 
  x=grconvertX(c(0.63,1.13), from='npc'),
  y=grconvertY(c(-0.18,0.32), from='npc'),
    type='fig', pars=list(mar=c(1.5,1.5,0,0)+0.1,mgp=c(0,0,0),tcl=0.2))


## Net Flux

NetFlux <-log((exp(ModelToPlotNP$TraitValue)*0.5-exp(ModelToPlotRes$TraitValue)*0.5))
par(new=TRUE)
plot(ModelToPlotNP$Temperature[1:Which],NetFlux[1:Which],type="l",ylab="",xlab="",lty=1,xaxt="n",yaxt="n",xlim=c(0,60),ylim=ylims,lwd=2)
par(new=FALSE)
mtext(1,text=expression("Temperature ("^{o}*C*")"),line=1.6)
mtext(4,text="log(NCF)",line=1.6)
ticks <- ticks
lab <- format(ticks,digits=2)
axis(side=4, at=ticks, labels=lab)

par(new=FALSE)

close.screen(all=TRUE)

dev.off()

####################################################################################


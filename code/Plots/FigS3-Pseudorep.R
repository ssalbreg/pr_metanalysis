
##################################################################################
### DENSITY PLOT ACTIVATION ENERGY: PSEUDOREPLICATION METHODS #####################

pdf(file="/Users/soflysb/ImperialCollege/PR_metanalysis/results/Plots/SI_Plots/FigS3_AqTer_EaPseudoRep.pdf",width=12,height=8,family="Times",pointsize=10)

library(asbio)


source("/Users/soflysb/ImperialCollege/PR_metanalysis/code/Plots/LabelColors.R")

par(mar = c(0.5,0.5,0.5,0.5) + 0.1,
          mgp=c(1.8,0.25,0),tcl=0.2)

split.screen(matrix(c(0,   1/3, 0.5, 1,
                      0,   1/3, 0,   0.5,
                      1/3, 2/3, 0.5, 1,  
                      1/3, 2/3, 0,   0.5,
                      2/3, 3/3, 0.5, 1,  
                      2/3, 3/3, 0,   0.5), ncol=4, byrow=TRUE))

par(oma = c(2.5,2.5,0,0) + 0.1)


##### WEIGHTED AVERAGE
######## Aquatic
source("/Users/soflysb/ImperialCollege/PR_metanalysis/code/Plots/WeightedAveE.R")
#R
EResp <- ERespA
Rem <- which(EResp>4)
if (length(Rem)>0){ EResp <- EResp[-Rem]}
MeanR <-mean(EResp)
set.seed(123)
BootR <- bootstrap(EResp,median,10000)
MedianR <- BootR$res[,1]
print(paste("meanR=",MeanR, "95%CI",unlist(t.test(EResp)[4]),"medianR=",MedianR, "bias=",BootR$res[,3],"95%CI",ci.boot(BootR)$res[4,],"n=",length(EResp)))

#P
EPho <- EPhoA
Rem <- which(EPho>4)
if (length(Rem)>0){ EPho <- EPho[-Rem]    }
MeanP <-mean(EPho)
set.seed(123)
BootP <- bootstrap(EPho,median,10000)
MedianP <- BootP$res[,2]
print(paste("meanP=",MeanP, "95%CI",unlist(t.test(EPho)[4]),"medianP=",MedianP, "bias=",BootP$res[,3],"95%CI",ci.boot(BootP)$res[4,],"n=",length(EPho)))

d <- density(EResp)
screen(1)
plot(d, xlab="",main="",ylim=c(0,1.65),xlim=c(0,3),xaxt="n")
polygon(d, ,xlab="",main="")
#mtext(1,text=substitute(paste(italic("E"), " (eV)")),line=1.8)
mtext(2,text="Density",line=1.6)
polygon(density(EResp), col=adjustcolor(RedResp, alpha.f=0.3), border=RedResp,lwd=3)
segments(x0=MedianR, x1=MedianR, y0=-10, y1=1.75, col=RedResp,lty=2,lwd=2)
#segments(x0=0.65, x1=0.65, y0=-10, y1=1.75, col="black",lty=4,lwd=2)
abline(v=seq(-0.5, 3, 0.1), col=rgb(0, 0, 0, 0.07), lwd=0.6)

d <- density(EPho)
polygon(density(EPho), col=adjustcolor(BlueNP, alpha.f=0.3), border=BlueNP,lwd=3)
segments(x0=MedianP, x1=MedianP, y0=-10, y1=1.75, col=BlueNP,lty=2,lwd=2)


#legend("topright",leg.txt1,bty="n",col=c("white","white"),cex=0.9)
legend("topleft","A",bty="n")
leg <- expression(paste("Aquatic ",italic('E')," distribution"))
legend("topright",leg,bty="n")
leg <- c(paste(),expression(),substitute(paste("n(",italic(R),")=",y,""),list(y=formatC(length(EResp), digits = 0,format = "f"))),substitute(paste("n(",italic(P),")=",y,""),list(y=formatC(length(EPho), digits = 0,format = "f"))))
legend("topright",leg,col=c(adjustcolor(RedResp,0.5),adjustcolor(BlueNP,0.5)),pch=19,bty="n",inset=c(0,0.07))

######## Ter
screen(2)

#R
EResp <- ERespT
Rem <- which(EResp>4)
if (length(Rem)>0){ ERespTT <- EResp[-Rem]}
MeanR <-mean(EResp)
BootR <- bootstrap(EResp,median,10000)
MedianR <- BootR$res[,2]
print(paste("meanR=",MeanR, "95%CI",unlist(t.test(EResp)[4]),"medianR=",MedianR, "bias=",BootR$res[,3],"95%CI",ci.boot(BootR)$res[4,],"n=",length(EResp)))

#P
EPho <- EPhoT
Rem <- which(EPho>4)
if (length(Rem)>0){ EPho <- EPho[-Rem]}
MeanP <-mean(EPho)
set.seed(123)
BootP <- bootstrap(EPho,median,10000)
MedianP <- BootP$res[,2]
print(paste("meanP=",MeanP, "95%CI",unlist(t.test(EPho)[4]),"medianP=",MedianP, "bias=",BootP$res[,3],"95%CI",ci.boot(BootP)$res[4,],"n=",length(EPho)))

d <- density(EResp)
plot(d, xlab="",main="",ylim=c(0,1.65),xlim=c(0,3))
mtext(2,text="Density",line=1.6)
mtext(1,text=substitute(paste(italic("E"), " (eV)")),line=1.6)
polygon(density(EResp), col=adjustcolor(RedResp, alpha.f=0.3), border=RedResp,lwd=3)
segments(x0=MedianR, x1=MedianR, y0=-10, y1=1.75, col=RedResp,lty=2,lwd=2)
#segments(x0=0.65, x1=0.65, y0=-10, y1=1.75, col="black",lty=4,lwd=2)
abline(v=seq(-0.5, 3, 0.1), col=rgb(0, 0, 0, 0.07), lwd=0.6)

d <- density(EPho)
polygon(density(EPho), col=adjustcolor(BlueNP, alpha.f=0.3), border=BlueNP,lwd=3)
segments(x0=MedianP, x1=MedianP, y0=-10, y1=1.75, col=BlueNP,lty=2,lwd=2)


#legend("topright",leg.txt1,bty="n",col=c("white","white"),cex=0.9)
legend("topleft","B",bty="n")
leg <- expression(paste("Terrestrial ",italic('E')," distribution"))
legend("topright",leg,bty="n")
leg <- c(paste(),expression(),substitute(paste("n(",italic(R),")=",y,""),list(y=formatC(length(EResp), digits = 0,format = "f"))),substitute(paste("n(",italic(P),")=",y,""),list(y=formatC(length(EPho), digits = 0,format = "f"))))
legend("topright",leg,col=c(adjustcolor(RedResp,0.5),adjustcolor(BlueNP,0.5)),pch=19,bty="n",inset=c(0,0.07))


################ HIGH FREQ HIGHR2
######## Aquatic
source("/Users/soflysb/ImperialCollege/PR_metanalysis/code/Plots/NonWeightedAveE-HighFreqHighR2.R")
#R
EResp <- ERespA
Rem <- which(EResp>4)
if (length(Rem)>0){ EResp <- EResp[-Rem]}
MeanR <-mean(EResp)
set.seed(123)
BootR <- bootstrap(EResp,median,10000)
MedianR <- BootR$res[,1]
print(paste("meanR=",MeanR, "95%CI",unlist(t.test(EResp)[4]),"medianR=",MedianR, "bias=",BootR$res[,3],"95%CI",ci.boot(BootR)$res[4,],"n=",length(EResp)))

#P
EPho <- EPhoA
Rem <- which(EPho>4)
if (length(Rem)>0){ EPho <- EPho[-Rem]    }
MeanP <-mean(EPho)
set.seed(123)
BootP <- bootstrap(EPho,median,10000)
MedianP <- BootP$res[,2]
print(paste("meanP=",MeanP, "95%CI",unlist(t.test(EPho)[4]),"medianP=",MedianP, "bias=",BootP$res[,3],"95%CI",ci.boot(BootP)$res[4,],"n=",length(EPho)))

d <- density(EResp)
screen(3)
plot(d, xlab="",main="",ylim=c(0,1.65),xlim=c(0,3),xaxt="n",yaxt="n")
polygon(d, ,xlab="",main="")
#mtext(1,text=substitute(paste(italic("E"), " (eV)")),line=1.8)
#mtext(2,text="Density",line=1.8)
polygon(density(EResp), col=adjustcolor(RedResp, alpha.f=0.3), border=RedResp,lwd=3)
segments(x0=MedianR, x1=MedianR, y0=-10, y1=1.75, col=RedResp,lty=2,lwd=2)
#segments(x0=0.65, x1=0.65, y0=-10, y1=1.75, col="black",lty=4,lwd=2)
abline(v=seq(-0.5, 3, 0.1), col=rgb(0, 0, 0, 0.07), lwd=0.6)

d <- density(EPho)
polygon(density(EPho), col=adjustcolor(BlueNP, alpha.f=0.3), border=BlueNP,lwd=3)
segments(x0=MedianP, x1=MedianP, y0=-10, y1=1.75, col=BlueNP,lty=2,lwd=2)


#legend("topright",leg.txt1,bty="n",col=c("white","white"),cex=0.9)
legend("topleft","C",bty="n")
leg <- expression(paste("Aquatic ",italic('E')," distribution"))
legend("topright",leg,bty="n")
leg <- c(paste(),expression(),substitute(paste("n(",italic(R),")=",y,""),list(y=formatC(length(EResp), digits = 0,format = "f"))),substitute(paste("n(",italic(P),")=",y,""),list(y=formatC(length(EPho), digits = 0,format = "f"))))
legend("topright",leg,col=c(adjustcolor(RedResp,0.5),adjustcolor(BlueNP,0.5)),pch=19,bty="n",inset=c(0,0.07))

######## Ter
screen(4)

#R
EResp <- ERespT
Rem <- which(EResp>4)
if (length(Rem)>0){ ERespTT <- EResp[-Rem]}
MeanR <-mean(EResp)
BootR <- bootstrap(EResp,median,10000)
MedianR <- BootR$res[,2]
print(paste("meanR=",MeanR, "95%CI",unlist(t.test(EResp)[4]),"medianR=",MedianR, "bias=",BootR$res[,3],"95%CI",ci.boot(BootR)$res[4,],"n=",length(EResp)))

#P
EPho <- EPhoT
Rem <- which(EPho>4)
if (length(Rem)>0){ EPho <- EPho[-Rem]}
MeanP <-mean(EPho)
set.seed(123)
BootP <- bootstrap(EPho,median,10000)
MedianP <- BootP$res[,2]
print(paste("meanP=",MeanP, "95%CI",unlist(t.test(EPho)[4]),"medianP=",MedianP, "bias=",BootP$res[,3],"95%CI",ci.boot(BootP)$res[4,],"n=",length(EPho)))

d <- density(EResp)
plot(d, xlab="",main="",ylim=c(0,1.65),yaxt="n",xlim=c(0,3))
mtext(1,text=substitute(paste(italic("E"), " (eV)")),line=1.8)
polygon(density(EResp), col=adjustcolor(RedResp, alpha.f=0.3), border=RedResp,lwd=3)
segments(x0=MedianR, x1=MedianR, y0=-10, y1=1.75, col=RedResp,lty=2,lwd=2)
#segments(x0=0.65, x1=0.65, y0=-10, y1=1.75, col="black",lty=4,lwd=2)
abline(v=seq(-0.5, 3, 0.1), col=rgb(0, 0, 0, 0.07), lwd=0.6)

d <- density(EPho)
polygon(density(EPho), col=adjustcolor(BlueNP, alpha.f=0.3), border=BlueNP,lwd=3)
segments(x0=MedianP, x1=MedianP, y0=-10, y1=1.75, col=BlueNP,lty=2,lwd=2)


#legend("topright",leg.txt1,bty="n",col=c("white","white"),cex=0.9)
legend("topleft","D",bty="n")
leg <- expression(paste("Terrestrial ",italic('E')," distribution"))
legend("topright",leg,bty="n")
leg <- c(paste(),expression(),substitute(paste("n(",italic(R),")=",y,""),list(y=formatC(length(EResp), digits = 0,format = "f"))),substitute(paste("n(",italic(P),")=",y,""),list(y=formatC(length(EPho), digits = 0,format = "f"))))
legend("topright",leg,col=c(adjustcolor(RedResp,0.5),adjustcolor(BlueNP,0.5)),pch=19,bty="n",inset=c(0,0.07))


################ HIGH FREQ HIGHR2
######## Aquatic
source("/Users/soflysb/ImperialCollege/PR_metanalysis/code/Plots/NonWeightedAveE.R")

#R
EResp <- ERespA
Rem <- which(EResp>4)
if (length(Rem)>0){ EResp <- EResp[-Rem]}
MeanR <-mean(EResp)
set.seed(123)
BootR <- bootstrap(EResp,median,10000)
MedianR <- BootR$res[,1]
print(paste("meanR=",MeanR, "95%CI",unlist(t.test(EResp)[4]),"medianR=",MedianR, "bias=",BootR$res[,3],"95%CI",ci.boot(BootR)$res[4,],"n=",length(EResp)))

#P
EPho <- EPhoA
Rem <- which(EPho>4)
if (length(Rem)>0){ EPho <- EPho[-Rem]    }
MeanP <-mean(EPho)
set.seed(123)
BootP <- bootstrap(EPho,median,10000)
MedianP <- BootP$res[,2]
print(paste("meanP=",MeanP, "95%CI",unlist(t.test(EPho)[4]),"medianP=",MedianP, "bias=",BootP$res[,3],"95%CI",ci.boot(BootP)$res[4,],"n=",length(EPho)))

d <- density(EResp)
screen(5)
plot(d, xlab="",main="",ylim=c(0,1.65),xlim=c(0,3),xaxt="n",yaxt="n")
polygon(d, ,xlab="",main="")
#mtext(1,text=substitute(paste(italic("E"), " (eV)")),line=1.8)
#mtext(2,text="Density",line=1.8)
polygon(density(EResp), col=adjustcolor(RedResp, alpha.f=0.3), border=RedResp,lwd=3)
segments(x0=MedianR, x1=MedianR, y0=-10, y1=1.75, col=RedResp,lty=2,lwd=2)
#segments(x0=0.65, x1=0.65, y0=-10, y1=1.75, col="black",lty=4,lwd=2)
abline(v=seq(-0.5, 3, 0.1), col=rgb(0, 0, 0, 0.07), lwd=0.6)

d <- density(EPho)
polygon(density(EPho), col=adjustcolor(BlueNP, alpha.f=0.3), border=BlueNP,lwd=3)
segments(x0=MedianP, x1=MedianP, y0=-10, y1=1.75, col=BlueNP,lty=2,lwd=2)


#legend("topright",leg.txt1,bty="n",col=c("white","white"),cex=0.9)
legend("topleft","E",bty="n")
leg <- expression(paste("Aquatic ",italic('E')," distribution"))
legend("topright",leg,bty="n")
leg <- c(paste(),expression(),substitute(paste("n(",italic(R),")=",y,""),list(y=formatC(length(EResp), digits = 0,format = "f"))),substitute(paste("n(",italic(P),")=",y,""),list(y=formatC(length(EPho), digits = 0,format = "f"))))
legend("topright",leg,col=c(adjustcolor(RedResp,0.5),adjustcolor(BlueNP,0.5)),pch=19,bty="n",inset=c(0,0.07))

######## Ter
screen(6)

#R
EResp <- ERespT
Rem <- which(EResp>4)
if (length(Rem)>0){ ERespTT <- EResp[-Rem]}
MeanR <-mean(EResp)
BootR <- bootstrap(EResp,median,10000)
MedianR <- BootR$res[,2]
print(paste("meanR=",MeanR, "95%CI",unlist(t.test(EResp)[4]),"medianR=",MedianR, "bias=",BootR$res[,3],"95%CI",ci.boot(BootR)$res[4,],"n=",length(EResp)))

#P
EPho <- EPhoT
Rem <- which(EPho>4)
if (length(Rem)>0){ EPho <- EPho[-Rem]}
MeanP <-mean(EPho)
set.seed(123)
BootP <- bootstrap(EPho,median,10000)
MedianP <- BootP$res[,2]
print(paste("meanP=",MeanP, "95%CI",unlist(t.test(EPho)[4]),"medianP=",MedianP, "bias=",BootP$res[,3],"95%CI",ci.boot(BootP)$res[4,],"n=",length(EPho)))

d <- density(EResp)
plot(d, xlab="",main="",ylim=c(0,1.65),yaxt="n",xlim=c(0,3))
mtext(1,text=substitute(paste(italic("E"), " (eV)")),line=1.8)
polygon(density(EResp), col=adjustcolor(RedResp, alpha.f=0.3), border=RedResp,lwd=3)
segments(x0=MedianR, x1=MedianR, y0=-10, y1=1.75, col=RedResp,lty=2,lwd=2)
#segments(x0=0.65, x1=0.65, y0=-10, y1=1.75, col="black",lty=4,lwd=2)
abline(v=seq(-0.5, 3, 0.1), col=rgb(0, 0, 0, 0.07), lwd=0.6)

d <- density(EPho)
polygon(density(EPho), col=adjustcolor(BlueNP, alpha.f=0.3), border=BlueNP,lwd=3)
segments(x0=MedianP, x1=MedianP, y0=-10, y1=1.75, col=BlueNP,lty=2,lwd=2)


#legend("topright",leg.txt1,bty="n",col=c("white","white"),cex=0.9)
legend("topleft","F",bty="n")
leg <- expression(paste("Terrestrial ",italic('E')," distribution"))
legend("topright",leg,bty="n")
leg <- c(paste(),expression(),substitute(paste("n(",italic(R),")=",y,""),list(y=formatC(length(EResp), digits = 0,format = "f"))),substitute(paste("n(",italic(P),")=",y,""),list(y=formatC(length(EPho), digits = 0,format = "f"))))
legend("topright",leg,col=c(adjustcolor(RedResp,0.5),adjustcolor(BlueNP,0.5)),pch=19,bty="n",inset=c(0,0.07))

close.screen(all=TRUE)

dev.off()

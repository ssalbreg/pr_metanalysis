
################################################################################
#########  INTRA-SPECIFIC COMPARISON for TERRESTRIAL SPECIES(SAME STUDY) ##########
################################################################################


source("/Users/soflysb/ImperialCollege/PR_metanalysis/code/Plots/RatePredicts.R")
source("/Users/soflysb/ImperialCollege/Ecoinformatics/Analysis/code/TPCFitting.R")
source("/Users/soflysb/ImperialCollege/PR_metanalysis/code/Plots/makeItalics.R")



############### ACTIVATION ENERGY #############

### Loads data and runs the function
load("/Users/soflysb/ImperialCollege/PR_metanalysis/data/TerDataFinalPlants.Rdata")

### Loads TResults
setwd("/Users/soflysb/ImperialCollege/PR_metanalysis/results/nlsLM_v0.4NoTref/")
TResults <- read.table("./results.csv",sep=",",header=TRUE)

TResults <- TResults[-which(TResults$r_sq_sch<0.5 | is.na(TResults$r_sq_sch)),] #Removes results where r2<0.5 or r2==NA
TResults <- TResults[-which(TResults$BefPk<3),] ## To simulate BA, that's what you  need at least 3 points, so we remove those with less


### Removes from TerData those ID that don't match with TResults$id
TerData <- TerData[-which(is.na(match(TerData$FinalID,TResults$id))),]

## Split data into respiration and photosynthesis
TResp <- TerData[grep("respiration rate",TerData$StandardisedTraitName,ignore.case=TRUE),]
TPhoto <- TerData[grep("net photosynthesis",TerData$StandardisedTraitName,ignore.case=TRUE),]


## Checks data to know how many studies we have where both R and P are measured
UniqueSpSt <- unique(paste(TerData$ConSpecies,TerData$Citation,sep="_"))
RP <- rep(NA,length(UniqueSpSt))
for (i in 1:length(UniqueSpSt)){
    WHICH <- which(paste(TerData$ConSpecies,TerData$Citation,sep="_")==UniqueSpSt[i])
Both <- unique(grepl("respiration rate",TerData$StandardisedTraitName[WHICH],ignore.case=FALSE))
if (length(Both)>1) RP[i] <- TRUE else RP[i] <- FALSE
}
length(which(RP==TRUE)) ## 103 studies where we have R & P

### Reduced Dataset

UniqueSpPR <- UniqueSpSt[which(RP==TRUE)]

### Matches results with TPC's and plots histograms for NET PHOTOSYNTHESIS
NetPE <- rep(NA,length(UniqueSpPR))
NetPB0 <- rep(NA,length(UniqueSpPR))
NetPUnit <- rep(NA,length(UniqueSpPR))

for (i in 1:length(UniqueSpPR)){
    WHICH <- which(!is.na(match(paste(TerData$ConSpecies,TerData$Citation,sep="_"),UniqueSpPR[i])))
    DataWhich <- WHICH[grep("net photosynthesis",TerData$StandardisedTraitName[WHICH],ignore.case=TRUE)]
    if(length(DataWhich)>0){
        ResID <- which(!is.na(match(as.character(TResults$id),unique(TerData$FinalID[DataWhich]))))
        if (length(ResID)>0){            
            Points <- table(TPhoto$FinalID[which(!is.na(match(TPhoto$FinalID,as.character(TResults$id[ResID]))))])
            NetPUnit[i] <- unique(TPhoto$StandardisedTraitUnit[which(!is.na(match(TPhoto$FinalID,as.character(TResults$id[ResID]))))])
            WW <- Points/mean(Points)
            NetPE[i] <- weighted.mean(TResults$E_sch[ResID],WW,na.rm=TRUE)
            NetPB0[i] <- weighted.mean(TResults$B0_renorm10[ResID],WW,na.rm=TRUE)
       
    }
    }
}

## Matches results with TPC's and plots histograms for Respiration
RE <- rep(NA,length(UniqueSpPR))
RUnit <- rep(NA,length(UniqueSpPR))
RB0 <- rep(NA,length(UniqueSpPR))
for (i in 1:length(UniqueSpPR)){
    WHICH <- which(!is.na(match(paste(TerData$ConSpecies,TerData$Citation,sep="_"),UniqueSpPR[i])))
    DataWhich <- WHICH[grep("respiration rate",TerData$StandardisedTraitName[WHICH],ignore.case=TRUE)]
    if(length(DataWhich)>0){
        ResID <- which(!is.na(match(TResults$id,unique(TerData$FinalID[DataWhich]))))
        if(length(ResID)>0){
         Points <- table(TResp$FinalID[which(!is.na(match(TResp$FinalID,as.character(TResults$id[ResID]))))])
            WW <- Points/mean(Points)
         RE[i] <- weighted.mean(TResults$E_sch[ResID],WW,na.rm=TRUE)
         RB0[i] <- weighted.mean(TResults$B0_renorm10[ResID],WW,na.rm=TRUE)
       RUnit[i] <- unique(TResp$StandardisedTraitUnit[which(!is.na(match(TResp$FinalID,as.character(TResults$id[ResID]))))])
       
        }
    }
}
EUniqueSpPR <- UniqueSpPR

## Matrix NetP, R for the same species
TotalE <- cbind(NetPE,RE,NetPB0,NetPUnit,RB0,RUnit)
##Calculates c
c <- NetPB0/RB0
TotalETer <- cbind(TotalE,c,"Ter")

#############################################################################
###############  TPEAK #############

### Loads data and runs the function
load("/Users/soflysb/ImperialCollege/PR_metanalysis/data/TerDataFinalPlants.Rdata")

### Loads TResults
setwd("/Users/soflysb/ImperialCollege/PR_metanalysis/results/nlsLM_v0.4NoTref/")
TResults <- read.table("./results.csv",sep=",",header=TRUE)

TResults <- TResults[-which(TResults$r_sq_sch<0.5 | is.na(TResults$r_sq_sch)),] #Removes results where r2<0.5 or r2==NA
#TResults <- TResults[-which(is.na(match(as.character(TResults$id),TerData$FinalID))),]

## ## Removes those curves for which the peak is not within the temperature range
## ID <- as.character(TResults$id)
## GoodPk <- rep(NA,length(ID))
## for (i in 1:length(ID)) {
    
##     TempR <- TerData$ConTemp[which(!is.na(match(TerData$FinalID,ID[i])))]
##     if (length(which(is.na(TempR)))==length(TempR)) TempR <- TerData$AmbientTemp[which(!is.na(match(TerData$FinalID,ID[i])))]
##     if(max(TempR) >= TResults$T_pk_sch[i]-273.15 && min(TempR) <= TResults$T_pk_sch[i]-273.15) GoodPk[i] <- TRUE else GoodPk[i] <- FALSE}

## TResults <- TResults[-which(GoodPk==FALSE),]


### Removes from TerData those ID that don't match with TResults$id
TerData <- TerData[-which(is.na(match(TerData$FinalID,TResults$id))),]

## Split data into respiration and photosynthesis
TResp <- TerData[grep("respiration rate",TerData$StandardisedTraitName,ignore.case=TRUE),]
TPhoto <- TerData[grep("net photosynthesis",TerData$StandardisedTraitName,ignore.case=TRUE),]


## Checks data to know how many studies we have where both R and P are measured

UniqueSpSt <- unique(paste(TerData$ConSpecies,TerData$Citation,sep="_"))
RP <- rep(NA,length(UniqueSpSt))
for (i in 1:length(UniqueSpSt)){
    WHICH <- which(paste(TerData$ConSpecies,TerData$Citation,sep="_")==UniqueSpSt[i])
Both <- unique(grepl("respiration rate",TerData$StandardisedTraitName[WHICH],ignore.case=FALSE))
if (length(Both)>1) RP[i] <- TRUE else RP[i] <- FALSE
}
length(which(RP==TRUE)) ## 103 studies where we have R & P

### Reduced Dataset

UniqueSpPR <- UniqueSpSt[which(RP==TRUE)]

## Matches results with TPC's and plots histograms for Respiration
NetPQF <- rep(NA,length(UniqueSpPR))    
NetPTpk <- rep(NA,length(UniqueSpPR))
for (i in 1:length(UniqueSpPR)){
  WHICH <- which(!is.na(match(paste(TerData$ConSpecies,TerData$Citation,sep="_"),UniqueSpPR[i])))
    DataWhich <- WHICH[grep("net photosynthesis",TerData$StandardisedTraitName[WHICH],ignore.case=TRUE)]
    if(length(DataWhich)>0){
        ResID <- which(!is.na(match(as.character(TResults$id),unique(TerData$FinalID[DataWhich]))))        
        if(length(ResID)>0){
            Points <- table(TPhoto$FinalID[which(!is.na(match(TPhoto$FinalID,as.character(TResults$id[ResID]))))]) ## For weighting average            
            print(length(Points))
              WW <- Points/mean(Points)
         NetPTpk[i] <- weighted.mean(TResults$T_pk_sch[ResID],WW,na.rm=TRUE)
                   
        
    }
}
}

RQF <- rep(NA,length(UniqueSpPR))    
RTpk <- rep(NA,length(UniqueSpPR))

for (i in 1:length(UniqueSpPR)){
  
WHICH <- which(!is.na(match(paste(TerData$ConSpecies,TerData$Citation,sep="_"),UniqueSpPR[i])))
DataWhich <- WHICH[grep("respiration rate",TerData$StandardisedTraitName[WHICH],ignore.case=TRUE)]
if(length(DataWhich)>0){
ResID <- which(!is.na(match(as.character(TResults$id),unique(TerData$FinalID[DataWhich]))))       
if(length(ResID)>0){
    Points <- table(TResp$FinalID[which(!is.na(match(TResp$FinalID,as.character(TResults$id[ResID]))))]) ## For weighting average            
    print(length(Points))

      WW <- Points/mean(Points)
      RTpk[i] <- weighted.mean(TResults$T_pk_sch[ResID],WW,na.rm=TRUE)
       
}
}
}

NPMin2 <- rep(NA,length(EUniqueSpPR))
NPPeak<- rep(NA,length(EUniqueSpPR))
NP2<- rep(NA,length(EUniqueSpPR))
NP5<- rep(NA,length(EUniqueSpPR))
RMin2<- rep(NA,length(EUniqueSpPR))
RPeak<- rep(NA,length(EUniqueSpPR))
R2<- rep(NA,length(EUniqueSpPR))
R5<- rep(NA,length(EUniqueSpPR))
CUEMin2<- rep(NA,length(EUniqueSpPR))
CUEPeak<- rep(NA,length(EUniqueSpPR))
CUE2<- rep(NA,length(EUniqueSpPR))
CUE5<- rep(NA,length(EUniqueSpPR))
TpkNP<- rep(NA,length(EUniqueSpPR))
TpkR<- rep(NA,length(EUniqueSpPR))
TpkNP_SD<- rep(NA,length(EUniqueSpPR))
TpkR_SD<- rep(NA,length(EUniqueSpPR))
TpkRealNP<- rep(NA,length(EUniqueSpPR))
TpkRealR<- rep(NA,length(EUniqueSpPR))
ENP<- rep(NA,length(EUniqueSpPR))
ER<- rep(NA,length(EUniqueSpPR))
B0NP<- rep(NA,length(EUniqueSpPR))
B0R<- rep(NA,length(EUniqueSpPR))
NPMin5<- rep(NA,length(EUniqueSpPR))
RMin5<- rep(NA,length(EUniqueSpPR))
CUEMin5<- rep(NA,length(EUniqueSpPR))

for (i in 1:length(EUniqueSpPR)){
    WHICH <- which(!is.na(match(paste(TerData$ConSpecies,TerData$Citation,sep="_"),UniqueSpPR[i])))
    NPP <- WHICH[grep("net photosynthesis",TerData$StandardisedTraitName[WHICH],ignore.case=TRUE)]
    Resp <- WHICH[grep("respiration rate",TerData$StandardisedTraitName[WHICH],ignore.case=TRUE)]
    
    if (length(NPP)>0 && length(Resp>0)) {            
        ResResults <- TResults[which(!is.na(match(TResults$id,unique(TerData$FinalID[Resp])))),]
        PhoResults <- TResults[which(!is.na(match(TResults$id,unique(TerData$FinalID[NPP])))),]        
        PointsNP <- table(TPhoto$FinalID[which(!is.na(match(TPhoto$FinalID,as.character(PhoResults$id))))])
PointsR <- table(TResp$FinalID[which(!is.na(match(TResp$FinalID,as.character(ResResults$id))))])
NPMin5[i] <- RatePredict(TerData,NPP,Resp,ResResults,PhoResults,PointsNP,PointsR)[1]
NPMin2[i] <- RatePredict(TerData,NPP,Resp,ResResults,PhoResults,PointsNP,PointsR)[2]
NPPeak[i] <- RatePredict(TerData,NPP,Resp,ResResults,PhoResults,PointsNP,PointsR)[3]
NP2[i] <- RatePredict(TerData,NPP,Resp,ResResults,PhoResults,PointsNP,PointsR)[4]
NP5[i] <- RatePredict(TerData,NPP,Resp,ResResults,PhoResults,PointsNP,PointsR)[5]
RMin5[i] <- RatePredict(TerData,NPP,Resp,ResResults,PhoResults,PointsNP,PointsR)[6]
RMin2[i] <- RatePredict(TerData,NPP,Resp,ResResults,PhoResults,PointsNP,PointsR)[7]
RPeak[i] <- RatePredict(TerData,NPP,Resp,ResResults,PhoResults,PointsNP,PointsR)[8]
R2[i] <- RatePredict(TerData,NPP,Resp,ResResults,PhoResults,PointsNP,PointsR)[9]
R5[i] <- RatePredict(TerData,NPP,Resp,ResResults,PhoResults,PointsNP,PointsR)[10]
CUEMin5[i] <- RatePredict(TerData,NPP,Resp,ResResults,PhoResults,PointsNP,PointsR)[11]
CUEMin2[i] <- RatePredict(TerData,NPP,Resp,ResResults,PhoResults,PointsNP,PointsR)[12]
CUEPeak[i] <- RatePredict(TerData,NPP,Resp,ResResults,PhoResults,PointsNP,PointsR)[13]
CUE2[i] <- RatePredict(TerData,NPP,Resp,ResResults,PhoResults,PointsNP,PointsR)[14]
CUE5[i] <- RatePredict(TerData,NPP,Resp,ResResults,PhoResults,PointsNP,PointsR)[15]
TpkNP[i] <- RatePredict(TerData,NPP,Resp,ResResults,PhoResults,PointsNP,PointsR)[16]
TpkR[i] <- RatePredict(TerData,NPP,Resp,ResResults,PhoResults,PointsNP,PointsR)[17]
TpkNP_SD[i] <- RatePredict(TerData,NPP,Resp,ResResults,PhoResults,PointsNP,PointsR)[18]
TpkR_SD[i] <- RatePredict(TerData,NPP,Resp,ResResults,PhoResults,PointsNP,PointsR)[19]
TpkRealNP[i] <- RatePredict(TerData,NPP,Resp,ResResults,PhoResults,PointsNP,PointsR)[20]
TpkRealR[i] <- RatePredict(TerData,NPP,Resp,ResResults,PhoResults,PointsNP,PointsR)[21]
ENP[i] <- RatePredict(TerData,NPP,Resp,ResResults,PhoResults,PointsNP,PointsR)[22]
ER[i] <- RatePredict(TerData,NPP,Resp,ResResults,PhoResults,PointsNP,PointsR)[23]
B0R[i] <- RatePredict(TerData,NPP,Resp,ResResults,PhoResults,PointsNP,PointsR)[24]
B0NP[i] <- RatePredict(TerData,NPP,Resp,ResResults,PhoResults,PointsNP,PointsR)[25]


    }
}

       

## Matrix NetP, R for the same species
TotalTpk <- cbind(NetPTpk,RTpk)
TotalTpk <- TotalTpk-273.15
TotalTpkTer <- cbind(TotalTpk,NetPQF,RQF)
CUETableTer <- cbind(NPMin5,NPMin2,NPPeak,NP2,NP5,RMin5,RMin2,RPeak,R2,R5,CUEMin5,CUEMin2,CUEPeak,CUE2,CUE5,TpkNP,TpkR,TpkNP_SD,TpkR_SD,TpkRealNP,TpkRealR,ENP,ER,B0R,B0NP)

save(TotalTpkTer,TotalETer,CUETableTer,file="/Users/soflysb/ImperialCollege/PR_metanalysis/data/TerPairDataRealPk.Rdata")

############## PLOT CURVES FOR R & NPP FROM THE SAME STUDY AND SPECIES ###

load("/Users/soflysb/ImperialCollege/PR_metanalysis/data/TerPairData.Rdata")
pdf(file="../Plots/IntraspPlots.pdf",width=14,height=8,family="Times")


par(mfrow=c(5,5))
ID <- 1:length(UniqueSpPR)
CUENP <- rep(NA,length(UniqueSpPR))
CUER <- rep(NA,length(UniqueSpPR))
CUENPWMin2 <- rep(NA,length(UniqueSpPR))
CUENPW2 <- rep(NA,length(UniqueSpPR))
CUENPW5 <- rep(NA,length(UniqueSpPR))
for (i in 1:length(UniqueSpPR))
    {
    NewData <- TerData
    WHICH <- which(!is.na(match(paste(NewData$ConSpecies,NewData$Citation,sep="_"),UniqueSpPR[ID[i]])))
    if (any(is.na(unique(NewData$ConTemp[WHICH])))) NewData$ConTemp[WHICH] <- as.numeric(NewData$AmbientTemp[WHICH])
    Resp <- WHICH[which(NewData$StandardisedTraitName[WHICH]=="respiration rate")]    
    NPP <- WHICH[which(NewData$StandardisedTraitName[WHICH]=="net photosynthesis")]
    if (length(NPP)>0 && length(Resp>0)) {
        if (length(unique(NewData$FinalID[Resp]))>1) Resp <- Resp[which(!is.na(match(NewData$FinalID[Resp],unique(NewData$FinalID[Resp])[1])))]
        if (length(unique(NewData$FinalID[NPP]))>1) NPP <- NPP[which(!is.na(match(NewData$FinalID[NPP],unique(NewData$FinalID[NPP])[1])))]
        NetData <-NewData[NPP,]
        ResData <- NewData[Resp,]

        ResR <- TResults[which(!is.na(match(TResults$id,unique(ResData$FinalID)))),]
        ResNPP <- TResults[which(!is.na(match(TResults$id,unique(NetData$FinalID)))),]
        print(paste(unique(NetData$StandardisedTraitUnit),ID[i],sep="_"))

        MinNPP <- min(NetData$StandardisedTraitValue,na.rm=TRUE)
        MinResp <- min(ResData$StandardisedTraitValue,na.rm=TRUE)

        if (MinNPP<0){
        NetData$StandardisedTraitValue <- NetData$StandardisedTraitValue-MinNPP
        NetData <- NetData[-which(NetData$StandardisedTraitValue==0),]}
        if (MinResp<0)
            {ResData$StandardisedTraitValue <- ResData$StandardisedTraitValue-MinResp
          ResData <- ResData[-which(ResData$StandardisedTraitValue==0),]}

   # Generate predictions from the model fit...
    ## RESP
    tmp_temps <- seq(min(
        floor(ResData$ConTemp+273.15)), 
                     ceiling(max(ResData$ConTemp+273.15)
                             ), length = 200)
    
    tmp_model <- exp(Schoolfield(
        ResR$lnB0_sch,
        ResR$E_sch,
        ResR$E_D_sch,
        ResR$T_h_sch,
        tmp_temps
        ))
    
    
    ModelToPlotRes <- data.frame(
        Temperature = tmp_temps - 273.15, 
        TraitValue = tmp_model*1000000
        )
    
    # Prepare the data points of the original values.
    DataToPlotRes <- data.frame(
        Temperature = ResData$ConTemp, 
        TraitValue = ResData$StandardisedTraitValue*1000000
        )
    DataToPlotRes <- na.omit(DataToPlotRes)

    ## PHOTO

      tmp_temps <- seq(min(
        floor(NetData$ConTemp+273.15)), 
                     ceiling(max(NetData$ConTemp+273.15)
                             ), length = 200)

       tmp_model <- exp(Schoolfield(
        ResNPP$lnB0_sch,
        ResNPP$E_sch,
        ResNPP$E_D_sch,
        ResNPP$T_h_sch,
        tmp_temps
        ))
    
    
    ModelToPlotNP <- data.frame(
        Temperature = tmp_temps - 273.15, 
        TraitValue = tmp_model*1000000
        )
    
    # Prepare the data points of the original values.
    DataToPlotNP <- data.frame(
        Temperature = NetData$ConTemp, 
        TraitValue = NetData$StandardisedTraitValue*1000000
        )
    DataToPlotNP <- na.omit(DataToPlotNP)

    #GPP <- log(exp(ModelToPlotNP$TraitValue)+exp(ModelToPlotRes$TraitValue))
    NF <- ModelToPlotNP$TraitValue*0.5-ModelToPlotRes$TraitValue*0.5
    NFLim <- NF
    if (length(which(!is.finite(NF)))>0){ NFLim <- NF[-which(!is.finite(NF))]}                        
    #### LIMS
    ylims <- range(min(c(DataToPlotNP$TraitValue,DataToPlotRes$TraitValue,ModelToPlotNP$TraitValue,ModelToPlotRes$TraitValue,NFLim)),max(c(DataToPlotNP$TraitValue,DataToPlotRes$TraitValue,ModelToPlotNP$TraitValue,ModelToPlotRes$TraitValue,NFLim)),na.rm=TRUE)
        ticks <- seq(from=0, to=1, length=3)
        lab <- format(ticks,digits=2)
        
    ### PLOT
   
    par(mar=c(3.25,4,2.25,2.75),mgp=c(0.5,0.5,0),tck=0.03)
        plot(DataToPlotRes$TraitValue~DataToPlotRes$Temperature,ylab="",xlab="",pch=19,cex=0.75,ylim=c(ylims),col=RedResp,xlim=c(0,60))

        if (any(unique(ID[i]==c(1,2,4,9,11,13:19,21:25))==TRUE)){
            mtext(2,text=expression(""*italic(mu)*"mol CO"[2]*" m"^{-2}*" s"^{-1}*""),line=1.5,cex=0.65)} else if  (any(unique(ID[i]==c(5:7))==TRUE)) {
           mtext(2,text=expression(""*italic(mu)*"mol CO"[2]*" Kg"^{-1}*" s"^{-1}*""),line=1.5,cex=0.65)}  else   if  (any(unique(ID[i]==c(8,10))==TRUE)) {                                   mtext(2,text=expression(""*italic(mu)*"mol CO"[2]*" Kg(DW)"^{-1}*" s"^{-1}*""),line=1.5,cex=0.65)}  else if  (any(unique(ID[i]==c(20))==TRUE)) {
        mtext(2,text=expression(""*italic(mu)*"L O"[2]*" exchanged m"^{-2}*" s"^{-1}*""),line=1.5,cex=0.65)  }
        mtext(1,text=expression("Temperature ("^{o}*C*")"),line=2,cex=0.65)
     Name <- make.italic(unique(NewData$ConSpecies[NPP]))
     mtext(3,text=Name,line=0.5,cex=0.55)
     
        
    Evalue <- c(paste(),expression(),substitute(paste("E"[Resp],"=",y,""),list(y=formatC(ResR$E_sch, digits = 2,format = "f"))),substitute(paste("E"[NP],"=",y,""),list(y=formatC(ResNPP$E_sch, digits = 2,format = "f"))))
    lines(ModelToPlotRes$Temperature,ModelToPlotRes$TraitValue,col=RedResp,lwd=1)
        
    
        legend("bottomright",Evalue,cex=0.85,bty="n")
        Max <- ResNPP$T_h_sch-273.15
        if (Max>max(NetData$ConTemp)) Max <- max(NetData$ConTemp)
        segments(x0=Max, x1=Max, y0=-30, y1=20000000000, col="black",lty=1)
        

         
        points(DataToPlotNP$TraitValue~DataToPlotNP$Temperature,pch=19,cex=0.75,col=BlueNP,ylim=ylims)
        lines(ModelToPlotNP$Temperature,ModelToPlotNP$TraitValue,col=BlueNP,lwd=1)     
        #lines(ModelToPlotNP$Temperature,GPP,col=GreenGP,lwd=1)    
      
        par(new=TRUE)
        plot(ModelToPlotNP$Temperature,NF,col="black",lty=2,type="l",xlab="",ylab="",xaxt="n",xlim=c(0,60),ylim=ylims)
        mtext(4,text="NCF",line=0.6,cex=0.65)
        axis(4,labels=FALSE)
        par(new=FALSE)
        
        ## x <- ModelToPlotNP$Temperature
        ## Val2 <- ModelToPlotNP$Temperature[which.max(ModelToPlotNP$TraitValue)]+2
        ## ValMin2 <- ModelToPlotNP$Temperature[which.max(ModelToPlotNP$TraitValue)]-2
        ## Val5 <- ModelToPlotNP$Temperature[which.max(ModelToPlotNP$TraitValue)]+5
        ## CUENPW2[i] <- CPU[which(abs(x-Val2)==min(abs(x-Val2)))]
        ## CUENPWMin2[i] <- CPU[which(abs(x-ValMin2)==min(abs(x-ValMin2)))]        
        ## CUENPW5[i] <- CPU[which(abs(x-Val5)==min(abs(x-Val5)))]
        ## CUENP[i] <- CPU[which.max(ModelToPlotNP$TraitValue)]     
        ## CUER[i] <- CPU[which.max(ModelToPlotRes$TraitValue)]
   
}}

dev.off()





#############################################################################
#### P&R dependencies on temperature for terrestrial data ##################


### TerData comes from file UnitConversionForP$RSubSet.R
### Here, that initial dataset is cleaned and standardized.
### There are several versions of the TerData file until get to the latest:
### V0.1- TerData.Rdata 
### V0.2- TerDataSatInf.Rdata (attaches CO2 and light saturation information)
### V0.3- TerDataFinal.Rdata (removes Atkin data)
### V0.4- TerDataFinalPlants.Rdata (removes fungi and is updated with the corresponding updates in the GlobalDataset.Rdata file vFebruary2017).





## ### Runs TPCFit function
## source("/Users/soflysb/ImperialCollege/Ecoinformatics/Analysis/code/TPCFitting.R")
## load("~/ImperialCollege/PR_metanalysis/data/TerData.Rdata")
## TerData$StandardisedTraitUnit[which(TerData$FinalID=="MTD3375")] <- "mol o2 Kg(Chl)^-1 s^-1" ## RUN THIS LINE NEXT TIME THIS IS UNCOMMENTED

## ## Updates template with last version (02/06/2016)
## ## Reads template
## Template <- as.character(read.table("/Users/soflysb/ImperialCollege/Ecoinformatics/Database/working-datasets/Template/BiotraitsTemplateFieldNames.csv",header=FALSE,sep=",")$V1)
## ## ## Match GLobalDataset with the template
## TerDataTemplate <- matrix(NA,ncol=length(Template),nrow=nrow(TerData))
## TerDataTemplate <- as.data.frame(TerDataTemplate)
## names(TerDataTemplate) <- Template
## MatchNames <- match(names(TerDataTemplate),names(TerData))
## TerDataTemplate[,which(!is.na(MatchNames))] <- TerData[,MatchNames[-which(is.na(MatchNames))]]
## Template[which(is.na(MatchNames))]
## TerDataTemplate[,c(77:80)] <- TerData[,c(75:78)]
## TerDataTemplate[,c(93:99)] <- TerData[,c(33:39)]
## TerDataTemplate[,c(130:133)] <- TerData[,c(125:128)]
## TerData <- TerDataTemplate
## save(TerData,file="~/ImperialCollege/PR_metanalysis/data/TerData.Rdata")
                               

## TerData$StandardisedTraitName[which(TerData$OriginalTraitName=="AnT")] <- "net photosynthesis"

## ##Adds a column to TerData for savig Saturating or not conditions
## Saturating <- rep(NA,length=nrow(TerData))
## TerData <- cbind(TerData,Saturating)

## ## Saves a file with unique citation. Here I'll fill the data for saturating CO2 and light
## write.csv(unique(TerData$Citation),"../data/UniCit.csv",row.names=FALSE)
## CL <- read.table("~/ImperialCollege/PR_metanalysis/data/CO2LightSat.csv",sep=";",header=TRUE,stringsAsFactors=FALSE)
## CL <- as.matrix(CL)
## CL[which(CL=="")] <- NA
## CL <- as.data.frame(CL)

## for (i in 1:length(unique(TerData$Citation)))
##      {
##     WHICH <- which(TerData$Citation==unique(TerData$Citation)[i])
##     WHICH <- WHICH[grep("photosyn",TerData$StandardisedTraitName[WHICH],ignore.case=TRUE)]
##     if(length(WHICH)>0){
##     if(any(!is.na(unique(TerData$AmbientLight[WHICH])))==FALSE){
##         TerData$AmbientLight[WHICH] <- as.character(CL$Light[i])
##         TerData$AmbientLightUnit[WHICH] <- as.character(CL$LightUnit[i])}
##     if(any(!is.na(unique(TerData$ResDenValue[WHICH])))==FALSE)
##         {
##     if (any(!is.na(unique(TerData$ResRepValue[WHICH])))==FALSE){
##             TerData$SecondStressor[WHICH] <- "CO2"
##             TerData$SecondStressorValue[WHICH] <- as.character(CL$CO2Con[i])
##             TerData$SecondStressorDef[WHICH] <- as.character(CL$CO2.1[i])
##             TerData$SecondStressorUnit[WHICH] <- as.character(CL$CO2Unit[i])
##         } else {
##               TerData$SecondStressorValue[WHICH] <- TerData$ResRepValue[WHICH]
##               TerData$SecondStressorUnit[WHICH] <- TerData$ResRepUnit[WHICH]
##           }
##     } else {
          
##           TerData$SecondStressorValue[WHICH] <- TerData$ResDenValue[WHICH]
##           TerData$SecondStressorUnit[WHICH] <- TerData$ResDenUnit[WHICH]
##       }

##     if (!is.na(CL$CO2Sat[i]) && !is.na(CL$LightSat[i])){
##         TerData$Saturating[WHICH] <- paste("Light,CO2",sep=",")}
##     if (is.na(CL$CO2Sat[i]) && !is.na(CL$LightSat[i])){
##         TerData$Saturating[WHICH] <- "Light"}
##     if (!is.na(CL$CO2Sat[i]) && is.na(CL$LightSat[i])){
##         TerData$Saturating[WHICH] <- "CO2"}

## }
## }

## ## Some of them have different values for different curves
## TerData$SecondStressorValue[which(TerData$Citation==unique(TerData$Citation)[5])][1:8] <- "36"
## TerData$SecondStressorDef[which(TerData$Citation==unique(TerData$Citation)[5])][1:8] <- "Ambient"
## TerData$SecondStressorValue[which(TerData$Citation==unique(TerData$Citation)[5])][9:19] <- "22"
## TerData$SecondStressorValue[which(TerData$Citation==unique(TerData$Citation)[5])][20:30] <- "60"
## TerData$SecondStressorDef[which(TerData$Citation==unique(TerData$Citation)[5])][9:30] <- "Internal"
## TerData$SecondStressorUnit[which(TerData$Citation==unique(TerData$Citation)[5])] <- "Pa"


## TerData$SecondStressorDef[which(TerData$Citation==unique(TerData$Citation)[33])] <- "Internal"
## TerData$SecondStressorDef[which(TerData$Citation==unique(TerData$Citation)[33])][which(is.na(TerData$SecondStressorValue[which(TerData$Citation==unique(TerData$Citation)[33])]))] <- NA
## TerData$Saturating[which(TerData$Citation==unique(TerData$Citation)[33])][which(TerData$SecondStressorValue[which(TerData$Citation==unique(TerData$Citation)[33])]=="500")] <- paste("Light,CO2",sep=",")

## TerData$Saturating[which(TerData$Citation==unique(TerData$Citation)[71])][22:52] <- paste("Light,CO2",sep=",")

## TerData$SecondStressorValue[which(TerData$Citation==unique(TerData$Citation)[86])] <- "385"
## TerData$SecondStressorValue[which(TerData$Citation==unique(TerData$Citation)[86])][which(TerData$FigureTable[which(TerData$Citation==unique(TerData$Citation)[86])]==
## "Fig2g")] <- "700"
## TerData$SecondStressorValue[which(TerData$Citation==unique(TerData$Citation)[86])][which(TerData$FigureTable[which(TerData$Citation==unique(TerData$Citation)[86])]==
## "Fig2h")] <- "700"

## TerData$SecondStressorValue[which(TerData$Citation==unique(TerData$Citation)[91])][which(TerData$FigureTable[which(TerData$Citation==unique(TerData$Citation)[91])]==
## "Fig3a")] <- "38"
## TerData$SecondStressorValue[which(TerData$Citation==unique(TerData$Citation)[91])][which(TerData$FigureTable[which(TerData$Citation==unique(TerData$Citation)[91])]==
## "Fig3b")] <- "76"


## ### Fixes some species names
## TerData$ConSpecies[which(TerData$ConSpecies=="Fagus silvatica")] <- "Fagus sylvatica"
## TerData$ConSpecies[which(TerData$ConSpecies=="Lycopersicon esculentum")] <- "Lycopersicum esculentum"


## ## SAVES TERDATA AFTER ADDING SAT INFORMATION
## save(TerData,file="~/ImperialCollege/PR_metanalysis/data/TerDataSatInf.Rdata")


## ## SAVES CSV FILE TO FILL INFO ABOUT TIMETESTTEMPERATURE etc.
## write.csv(TerData,file="~/ImperialCollege/PR_metanalysis/data/TerDataSatInf.csv")

## ## READS TerDataSatInf, containing the info about TimeTestTemperature 

## TerData <- read.csv("~/ImperialCollege/PR_metanalysis/data/TerDataSatInf.csv",header=TRUE,stringsAsFactors=FALSE)

## TerData$ConObsTimeValue[which(TerData$ConObsTimeValue=="6 days")] <- 6
## TerData$ConObsTimeUnit[which(TerData$ConObsTimeValue=="6 days")] <- "days"

## ### Converts to SI columns of Obs and EquilibTime
## TerData$ConEquilibTimeValue[which(as.character(TerData$ConEquilibTimeValue)=="not given in text")] <- NA
## TerData$ConEquilibTimeValue[which(as.character(TerData$ConEquilibTimeValue)=="")] <- NA
## TerData$ConEquilibTimeValue <- as.numeric(TerData$ConEquilibTimeValue)
## TerData$ConEquilibTimeUnit <- as.character(TerData$ConEquilibTimeUnit)

## TerData$ConEquilibTimeValue[which(TerData$ConEquilibTimeUnit=="days" | TerData$ConEquilibTimeUnit=="day")] <- TerData$ConEquilibTimeValue[which(TerData$ConEquilibTimeUnit=="days" | TerData$ConEquilibTimeUnit=="day")]*24*60*60
## TerData$ConEquilibTimeUnit[which(TerData$ConEquilibTimeUnit=="days" | TerData$ConEquilibTimeUnit=="day")] <- "seconds"

## TerData$ConEquilibTimeValue[which(TerData$ConEquilibTimeUnit=="minutes" | TerData$ConEquilibTimeUnit=="min")] <- TerData$ConEquilibTimeValue[which(TerData$ConEquilibTimeUnit=="minutes" | TerData$ConEquilibTimeUnit=="min")]*60
## TerData$ConEquilibTimeUnit[which(TerData$ConEquilibTimeUnit=="minutes" | TerData$ConEquilibTimeUnit=="min")] <- "seconds"

## TerData$ConEquilibTimeValue[which(TerData$ConEquilibTimeUnit=="hours" | TerData$ConEquilibTimeUnit=="h" | TerData$ConEquilibTimeUnit=="hr" | TerData$ConEquilibTimeUnit=="hour")] <- TerData$ConEquilibTimeValue[which(TerData$ConEquilibTimeUnit=="hours" | TerData$ConEquilibTimeUnit=="h" | TerData$ConEquilibTimeUnit=="hr" | TerData$ConEquilibTimeUnit=="hour")]*60*60
## TerData$ConEquilibTimeUnit[which(TerData$ConEquilibTimeUnit=="hours" | TerData$ConEquilibTimeUnit=="h" | TerData$ConEquilibTimeUnit=="hr" | TerData$ConEquilibTimeUnit=="hour")]  <- "seconds"

## TerData$ConObsTimeValue[which(as.character(TerData$ConObsTimeValue)=="not given in text")] <- NA
## TerData$ConObsTimeValue[which(as.character(TerData$ConObsTimeValue)=="")] <- NA
## TerData$ConObsTimeUnit[which(as.character(TerData$ConObsTimeUnit)=="not stated")] <- NA

## TerData$ConObsTimeValue <- as.numeric(TerData$ConObsTimeValue)
## TerData$ConObsTimeUnit <- as.character(TerData$ConObsTimeUnit)

## TerData$ConObsTimeValue[which(TerData$ConObsTimeUnit=="minutes" | TerData$ConObsTimeUnit=="min")] <- TerData$ConObsTimeValue[which(TerData$ConObsTimeUnit=="minutes" | TerData$ConObsTimeUnit=="min")]*60
## TerData$ConObsTimeUnit[which(TerData$ConObsTimeUnit=="minutes" | TerData$ConObsTimeUnit=="min")] <- "seconds"

## TerData$ConObsTimeValue[which(TerData$ConObsTimeUnit=="hours" | TerData$ConObsTimeUnit=="hr" | TerData$ConObsTimeUnit=="hour")] <- TerData$ConObsTimeValue[which(TerData$ConObsTimeUnit=="hours" | TerData$ConObsTimeUnit=="hr" | TerData$ConObsTimeUnit=="hour")]*60*60
## TerData$ConObsTimeUnit[which(TerData$ConObsTimeUnit=="hours" |  TerData$ConObsTimeUnit=="hr" | TerData$ConObsTimeUnit=="hour")]  <- "seconds"

## TerData$ConObsTimeUnit[which(TerData$ConObsTimeUnit=="")]  <- NA
## TerData$ConEquilibTimeUnit[which(TerData$ConEquilibTimeUnit=="")]  <- NA

## #### SAVES CSV FILE AFTER CHANGES
## write.csv(TerData,file="~/ImperialCollege/PR_metanalysis/data/TerDataSatInf.csv")
## save(TerData,file="~/ImperialCollege/PR_metanalysis/data/TerDataSatInf.Rdata")

## ## ### READS TerDataSatInf, once information has been added
## # TerData <- read.csv("~/ImperialCollege/PR_metanalysis/data/TerDataSatInf.csv",header=TRUE,stringsAsFactors=FALSE)

## TerData <- TerData[-which(TerData$SubmittedBy=="Dr. Owen Atkin"),] ## Removes Atkin and runs the code without it

## ## O'Sullivan and Heskel are also supposed to be ramping data, so I remove them as well
## TerData <- TerData[-which(TerData$Citation=="O'Sullivan, O. S., Weerasinghe, K. W. L. K., Evans, J. R., Egerton, J. J. G., Tjoelker, M. G., & Atkin, O. K. (2013). High-resolution temperature responses of leaf respiration in snow gum (Eucalyptus pauciflora) reveal high-temperature limits to respiratory function. Plant, Cell and Environment, 36, 1268_1284. http://doi.org/10.1111/pce.12057"),]
## TerData <- TerData[-which(TerData$Citation=="Heskel, M. A., Greaves, H. E., Turnbull, M. H., O'Sullivan, O. S., Shaver, G. R., Griffin, K. L., & Atkin, O. K. (2014). Thermal acclimation of shoot respiration in an Arctic woody plant species subjected to 22\303\246years of warming and altered nutrient supply. Global Change Biology, 20(8), 2618_2630. http://doi.org/10.1111/gcb.12544"),]
## TerData <- TerData[-which(TerData$Citation=="Gauthier, P.P.G., Crous, K.Y., Ayub, G., Duan, H., Weerasinghe, L.K., Ellsworth, D.S., et al. (2014). Drought increases heat tolerance of leaf respiration in Eucalyptus globulus saplings grown under both ambient and elevated atmospheric [CO2] and temperature. J. Exp. Bot., 65, 6471_6485."),]

## ## Saves TerData without Atkins
# save(TerData,file="/Users/soflysb/ImperialCollege/PR_metanalysis/data/TerDataFinal.Rdata")

## ### Loads data and runs the function
## load("/Users/soflysb/ImperialCollege/PR_metanalysis/data/TerDataFinal.Rdata")

## ## Matches taxonomy after updating the GlobalDatabase file and removes fungi data
## load("/Users/soflysb/ImperialCollege/Ecoinformatics/Database/working-datasets/GlobalDataset/GlobalDataset.Rdata")
## MATCH <- match(TerData$FinalID,GlobalDataset$FinalID)
## TerData[,62:68] <- GlobalDataset[MATCH,62:68]
## TerData <- TerData[-which(TerData$ConKingdom=="Fungi" | TerData$ConKingdom=="Metazoa"),]
## save(TerData,file="/Users/soflysb/ImperialCollege/PR_metanalysis/data/TerDataFinalPlants.Rdata")

### Loads data and runs the function
load("/Users/soflysb/ImperialCollege/PR_metanalysis/data/TerDataFinalPlants.Rdata")
setwd("/Users/soflysb/ImperialCollege/PR_metanalysis/results/nlsLM_v0.4NoTref/")
TPCFit(Data=TerData,OverPLOT=FALSE,Model="Schoolfield",SchoolTpk=TRUE,PLOT=TRUE,rand.st=TRUE,n.rand=100,temper="ConTemp",ID="FinalID",trait="StandardisedTraitValue")

TResults <- read.table("./results.csv",sep=",",header=TRUE)

##### Adds renormalization BO at 10
B0 <- rep(NA,length=nrow(TResults))
for (i in 1:nrow(TResults)){
B0[i] <- exp(Schoolfield(TResults$lnB0_sch[i],TResults$E_sch[i], TResults$E_D_sch[i], TResults$T_h_sch[i], 283.15, SchoolTpk=TRUE))}
B0_renorm10 <- B0
TResults <- cbind(TResults,B0_renorm10)
write.csv(TResults,"./results.csv")

##setwd("/Users/soflysb/ImperialCollege/PR_metanalysis/results/nlsLM_v0.3NoTref/") # Old version before minor change in the code (random calculation of lnBst)

### From here this data is used in the corresponding codes for Plots.



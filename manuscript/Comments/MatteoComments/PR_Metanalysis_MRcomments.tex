\documentclass[11pt]{article}
\usepackage[margin = 1in]{geometry}
\setlength{\headheight}{10pt}
\usepackage{paralist}
\setlength{\parindent}{1em}
\setlength{\parskip}{0em}

\usepackage{gensymb}
\usepackage{amsmath}
\usepackage{multirow}
\usepackage{url}
\usepackage{pdflscape}
\usepackage[labelfont=bf]{caption}
\usepackage{graphicx} % for the figures
\usepackage[mathlines]{lineno} % for line numbers
\pagenumbering{arabic} % stating the page number type
\linenumbers
\usepackage{lmodern}
\usepackage{natbib}
\setlength{\bibsep}{0.0pt}
\renewcommand{\bibfont}{\scriptsize}

%%%%%%%%% Keep this block in manuscript to allow commenting %%%%%%%%%
\usepackage{color}
\usepackage{setspace}
\usepackage{soul} %to get highlights with todo notes
\usepackage[backgroundcolor=yellow,textsize=tiny,textwidth=.9in]{todonotes}
% {\begin{spacing}{0.5}#2\end{spacing}}
\newcounter{todocounter}
\newcommand{\todonum}[2][]
{\stepcounter{todocounter}\todo[#1]{\thetodocounter: #2}}
\newcommand{\hlfix}[2]{\texthl{#1}\todonum{#2}~} %this needs the soul package
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%% END OF PREAMBLE %%%%%%%%%%%%%%%%

\begin{document}

\title{	{\Large \bf Key generalities and deviations in the thermal response of organismal respiration and photosynthesis} }
\author{Sof\'{i}a Sal$^{1*}$, Gabriel Yvon-Durocher$^{2}$, Bernardo Garc\'{i}a-Carreras$^{1}$,\\
 Richard Sheppard$^{1}$, Matteo Rizzuto$^{1,3}$ and Samraat Pawar$^{1}$}

\maketitle
\normalsize{$^{1}$Department of Life Sciences, Imperial College London, Silwood Park, Ascot, Berkshire SL5 7PY, UK\\}
\normalsize{$^{2}$Environment and Sustainability Institute, University of Exeter, Penryn, Cornwall, TR10 9EZ. UK\\}
\normalsize{$^{3}$Department of Biology, Memorial University of Newfoundland, St. John's, Newfoundland and Labrador, Canada\\}

\normalsize{$^{*}$Corresponding author: s.sal-bregua@imperial.ac.uk\\}

\clearpage

{\bf The combined metabolic activity of aquatic and terrestrial
autotrophs profoundly impacts the global carbon balance through the
flux of carbon dioxide (CO$_2$) between ecosystems and the
atmosphere \citep{Arneth2010, IPCC2013, Cole2007}. Current
ecosystem models typically assume that the respiratory release of
CO$_2$ by autotrophs both responds more rapidly (greater thermal
sensitivity), and peaks at a higher temperature (higher thermal
optimum) than photosynthetic fixation of carbon, leading to
predictions of an  increase in net loss (efflux) of carbon to the atmosphere with climatic
warming \citep{Allen2005, Yvon-Durocher2010, Salvucci2004,
Huve2011, OSullivan2013}. However, these assumed differences in thermal
response of respiration and photosynthesis rates have not been
validated using a systematic comparison of species-level data across
habitats and taxonomic groups. Here, we build and analyse a global
database of species-level photosynthesis and respiration thermal
performance curves across both terrestrial and aquatic habitats to
quantify the differences in their thermal responses, and the resulting
impact on ecosystem carbon use efficiency and flux. We find that
respiration on average peaks at a significantly higher temperature than
photosynthesis across habitats ($\approx$10.3$^{\circ}$C difference in
terrestrial and $\approx$7.5$^{\circ}$C in aquatic species). However,
activation energy of respiration is not as consistently higher than
that of photosynthesis as previously assumed \citep{Enquist2003a,
Brown2004b, Allen2005, Yvon-Durocher2010}, with this difference being
highly variable in aquatic autotrophs. Using these general empirical
patterns, we then show that carbon use efficiency --- the fraction of
sequestered carbon used for growth ---, and net carbon gain (influx) are both
likely to decline with global warming in aquatic as well as terrestrial
ecosystems. Because the differences in activation energy of
photosynthesis and respiration are narrowly distributed in terrestrial
autotrophs, this decline can be predicted mainly by differences in
their thermal optima. In contrast, both activation energy and thermal
optimum differences will likely contribute to the loss of CUE and
carbon balance in aquatic ecosystems. These key generalities and
deviations in the thermal sensitivity of species-level respiration and
photosynthesis rates provides a basis for better predicting the future
effects of climate warming on carbon balance across aquatic and
terrestrial ecosystems, and from local to global scales.}

\todo{MR: there is a lot of brackets here. No way to get rid of some?}Ecosystem carbon balance depends on differences in the thermal response
of respiration (which releases carbon) and photosynthesis (which fixes
carbon) rates of both aquatic (phytoplankton and macrophytes) and
terrestrial (plants) autotrophs. \todo{MR: This is a beast of a sentence, could it be broken in 2? E.g., first sentence defines thermal sensitivity, 2nd sentence exlpains the assumption}Current models of ecosystem carbon
balance assume that both thermal sensitivity --- the rate of change in
an organism's metabolic rate with a unit change in temperature within
its operational temperature range
(\todo{MR:did you mean to have an abbreviation here?}OTR\citep{Pawar2016,Angilletta2002}, Fig.\ \ref{fig:tpcEx}) --- and the
optimum of the thermal performance curve (TPC) of respiration is higher
than that of photosynthesis. Specifically, it has been suggested that
``activation energy'' ($E$; Fig.\ \ref{fig:tpcEx}), which quantifies
thermal sensitivity, of autotroph respiration is generally higher than
that of photosynthesis ($E_R \approx$ 0.65 eV vs.\ $E_{P_\text{net}}$
$\approx 0.32$ eV) \citep{Allen2005, Lopez-Urrutia2006c,
Anderson-Teixeira2011, Yvon-Durocher2012a}. \todo{lots of adverbs. If you have words to spare, maybe use ``In a similar way''?}Similarly, evidence
suggests that the temperature at which the rate is maximised
($T_\text{pk}$) is higher for respiration than for photosynthesis
\citep{Huve2011, OSullivan2013}. The effects of both these differences
on organismal physiology and ecosystem carbon balance can be quantified
using two key measures: Carbon Use Efficiency (CUE) and net carbon
flux. CUE quantifies organismal growth efficiency \citep{Padfield2016} as
well as ecosystem productivity \citep{DeLucia2007} and is defined as
the ratio between respiration ($R$) and gross photosynthesis ($P_\text{gross}$):
\begin{equation}\label{eq:CUE}
	\textrm{CUE} = 1 - \frac{R}{P_\textrm{gross}}
\end{equation}
Net carbon flux quantifies the rate of organismal and ecosystem carbon
gain or loss due to metabolism, and corresponds to the difference
between diurnal net photosynthesis and nocturnal respiration rates,
\begin{equation}\label{eq:NetFlux}
	\textrm{Net flux} = P_\text{net} - R
\end{equation}
where $P_\text{net}$ is the difference between  $P_\text{gross}$ and
diurnal respiration. Substituting the temperature dependencies of
Photosynthesis and Respiration rates \todo{MR: I couldn't find details on this in the Methods. Am I missing something?}(see Methods) into Eqns
\ref{eq:CUE} and \ref{eq:NetFlux} yield predictions of
the instantaneous response of these key measures to temperature change
(Fig. \ref{fig:tpcEx}). As illustrated in Fig. \ref{fig:tpcEx}, these
responses depend both on the differences in thermal sensitivities (activation
energies, $E$) and thermal optima ($T_\text{pk}$) of the TPCs of respiration and
photosynthesis. Assuming a higher activation energy for respiration
rate compared to photosynthesis, as past studies have done, would imply
that both CUE and net carbon gain might be expected to decline with
increases in temperature \citep{Gifford2003, Allison2010} (Fig.\
\ref{fig:tpcEx}A). However, if the activation energies of both rates
are equal, CUE and net carbon gain can still decline due to differences in the $T_\text{pk}$ of respiration and photosynthesis (Fig. \ref{fig:tpcEx}B).

These differences between respiration and photosynthesis TPCs -- hereafter, defined as mismatches -- are based on across-species (interspecific) studies, where the activation energy is estimated from a single measurement per-species \citep{Allen2005, Lopez-Urrutia2006c, Anderson-Teixeira2011, Yvon-Durocher2012a}. This assumes that there is a universal temperature dependence across species \citep{Gillooly2001}, with all measures falling on a single curve. In contrast, a more appropriate test for the differences in thermal sensitivity of these rates at the individual level requires that activation energies be estimated separately for each species using data on metabolic rates of individuals across a range of temperatures (intraspecific studies; Fig. \ref{fig:tpcEx}) \citep{Dell2011}. Such an analysis has not yet been attempted because of the lack of sufficient data for a comparative analysis across taxa and habitats \citep{Huey1989, Huey2001, Angilletta2002}.

Here, we compile and analyze a new global database of photosynthesis and respiration thermal performance curves of 293 autrophs species from terrestrial and aquatic habitats. To characterize the $E$ and $T_\text{pk}$ of both respiration and photosynthesis we fit a modification of the \todo{MR: I'm sure it's just me, but why this model exactly?}Sharpe-Schoolfield model \citep{Schoolfield1981} \citep{Levenberg1944, Marquardt1963} (see Methods). We test whether (i) respiration is indeed more sensitive to temperature than photosynthesis, and (ii) photosynthesis and respiration rates peak at different temperatures. We do so by comparing the distributions of parameters for photosynthesis and respiration rates (i) across all species (global comparison), and (ii) for the subset of those data for which both rates were measured for the same organism in the same study (within-species comparison). For both datasets, we do also a comparison across ecologically meaningful subsets (i.e., aquatic and terrestrial organisms) but also for specific groups (C3-C4 for terrestrial plants and macrophytes-phytoplankton for aquatic organisms). Finally, using the results obtained in the within-species comparison, we analyse the consequences of the observed differences on the carbon use efficiency and net carbon gain in the context of the ongoing climate warming.

\todo{MR:The jump-in to your results is a bit stark. Took me a second read to realise this is where you start reporting your findings. Maybe add a couple of words to highlight that it is the distributions of E in your dataset?}The distributions of $E$ (Fig. \ref{fig:ehist}A-B) are right skewed, as has been previously observed \citep{Dell2011}. This is consistent across habitats and for both $P_\text{net}$ and $R$, meaning that the majority of responses have lower activation energies than the mean value of the distribution (see Fig. \ref{fig:ehist}A-B), and the median value is more representative of the distribution \citep{Dell2011}. There is no difference in global median $E$ for $P_\text{net}$ and $R$. This result contradicts the current understanding that $E$ for respiration is greater than that for photosynthesis. Fig.~\ref{fig:ehist}A-B shows that median $E_{P_\text{net}}$ and $E_R$ are also not significantly different in terrestrial ($E_{P_\text{net}}$: 0.61 eV, CI: 0.53-0.61 vs.\ $E_{R}$: 0.64 eV, CI: 0.62-0.69) or aquatic species ($E_{R}$: 0.87 eV, CI: 0.65-1.18 vs.\ $E_{P_\text{net}}$: 0.70 eV, CI: 0.56-0.96). The median value of $E$ remains similar across habitats (Table S1) \todo{MR:the difference in median values for Phylo seems large. Is it really ns?}but also across the major groups within both habitats (Fig. S1). In most cases, the 95\% confidence intervals around $E$ include the value of 0.65eV reported for respiration but not the value of 0.32 eV previously proposed for net photosynthesis \citep{Allen2005, Lopez-Urrutia2006c, Anderson-Teixeira2011, Yvon-Durocher2012a}.

In contrast to $E$, the $T_\text{pk}$ distributions for respiration and net photosynthesis are centrally distributed in both terrestrial and aquatic ecosystems (Fig. \ref{fig:ehist}C-D). Both, the global mean and median $T_\text{pk}$ are significantly higher for R than NP \todo{MR: are R and NP interchangeable with $R$ and $P_{net}$? If so, it might be worth stating it somewhere because, as it is, R and NP pop up out of nowhere and are at times confusing}(Fig.~\ref{fig:ehist}C-D, Table S1). The same pattern holds for aquatic (median $T_{\text{pk},R}$: 31.21$^{\circ}$C, CI: 29.59-33.64 vs.\ median $T_{\text{pk},P_\text{net}}$: 25.31$^{\circ}$C, CI: 24.12-26.64) and terrestrial species (median $T_{\text{pk},R}$: 35.10$^{\circ}$C, CI: 33.88-38.54 vs median $T_{\text{pk},P_\text{net}}$: 27.37$^{\circ}$C, CI: 25.29-28.95; Fig.~\ref{fig:ehist}C-D).

%% Although for both NP and R, the differences in $T_\text{pk}$ for aquatic and terrestrial species are not significantly different, median values for terrestrial species are higher than in aquatic species,  which could be due to the fact that terrestrial species experience higher temperature fluctuations (REF) and $T_\text{pk}$ is always closer to the mean environmental temperature \citep{Slatyev1978, Berry1980}.
%%\todonum{BGC: I would be tempted to delete these last two sentences, given that at the end of the day there's no statistical significance (so there's little point in reading into it). SSB: Maybe you're right, I've commented them for now}

When directly comparing within-species pairs of activation energies of $P_\text{net}$ and $R$, $E_R$ was higher than $E_{P_\text{net}}$ in 73\% of cases (64\% for aquatic and 83\% for terrestrial species; Fig. \ref{fig:scatter}A). The median of the differences between $E_R$ and $E_{P_\text{net}}$ for aquatic species was on average 0.14 eV, but not significantly different from zero (Wilcoxon signed-rank test: \textit{V}=243, \textit{p}=0.37), while the opposite was true for terrestrial species (median of the differences was 0.25 eV; \textit{V}=251, \textit{p}=0.003). The differences in $E$ were however distinctly more variable in aquatic ($\sigma=0.93$ eV) than in terrestrial species ($\sigma=0.32$ eV; Fig.~\ref{fig:scatter}C) according to Levene's test (F = 12.46, p $<$ 0.001). On the other hand, $R$ peaked at higher temperatures than $P_\text{net}$ in about 91\% of cases (100\% for aquatic and 71\% for terrestrial species; Fig.~\ref{fig:scatter}B). The median of the differences ($T_{\text{pk},R} - T_{\text{pk},P_\text{net}}$) was 10.26$^{\circ}$C (Wilcoxon signed-rank test: \textit{V}=20, \textit{p}=0.37) for terrestrial and 6.69$^{\circ}$C (\textit{V} =136, \textit{p} = 3.05E-05) for aquatic species (Table S2). Given the results observed across species (Fig.~\ref{fig:ehist}D), the fact that the median of the differences is not significant for terrestrial species, is due to the reduced number of pairs of data available in the within-species comparison (n=7) (notice that in a more conservative subset (Fig. S6) the difference is significant for terrestrial species (see Table S2)).

To explore how observed differences in $E$ and $T_\text{pk}$ of photosynthesis and respiration affect ecosystems' response to temperature, we estimated CUE and net carbon gain for each species for which both rates were measured in the same experiment, at temperatures relative to each species' own $T_{\text{pk},P_\text{net}}$ (Fig.~\ref{fig:CUE}). We chose $T_{\text{pk},P_\text{net}}$ as the reference point because $P_\text{net}$ peaks at lower temperatures than $R$ (Figs.~\ref{fig:ehist}C-D and \ref{fig:scatter}B), and is more likely to constitute the upper limit of the OTR (Fig.~\ref{fig:tpcEx}) for most species. As shown in Fig.~\ref{fig:CUE}A and D, both CUE and net carbon gain suffer greater declines with increases in temperature for species living at $T > T_{\text{pk},P_\text{net}}$, i.e. outside the OTR. Most species, however, live within their OTR \citep{Pawar2016,Angilletta2002}, and for these, CUE is likely to decline more moderately with rising temperatures, and their influx may even increase.

At an individual scale we observed systematic mismatches in $T_\text{pk}$, but not in $E$. As a result, changes in CUE and net carbon gain might be expected to be driven by differences in $T_\text{pk}$. While the change in CUE due to an increase in temperature was negatively correlated with the difference in the underlying $T_\text{pk}$ for $P_\text{net}$ and $R$, the relationship was significant only for terrestrial species (Fig.~\ref{fig:CUE}B-C). The differences in $E$ for terrestrial species were narrowly distributed (Fig.~\ref{fig:scatter}C), meaning that changes in CUE are more likely to be exclusively driven by differences in $T_\text{pk}$ (as depicted in Fig.~\ref{fig:tpcEx}B). The distribution of differences in $E$ for aquatic species is, however, wider, meaning that within individual organisms, there was a greater likelihood for $E_R$ and $E_{P_\text{net}}$ to be different. In this case, both differences in $T_\text{pk}$ and $E$ can affect CUE (e.g., Fig.~\ref{fig:tpcEx}A).

Our study throws new light on the understanding of the intraspecific
thermal responses of two major biological traits: respiration rate and net photosynthesis. We show that, contrary to what has been shown in previous (interspecific) studies \citep{Allen2005, Lopez-Urrutia2006c, Anderson-Teixeira2011, Yvon-Durocher2012a}, $E$ for respiration and photosynthesis are not significantly different, when all species are combined together, for both aquatic and terrestrial habitats. However, the within-species comparison reveals a novel pattern, where for terrestrial species only, respiration is more sensitive to temperature than net photosynthesis -- although that difference is much lower (0.25 eV as median) than that assumed by the canonical values (a difference of 0.33 eV) -- but this difference is not statistically significant for aquatic species. Differences between $T_{\text{pk},P_\text{net}}$ and $T_{\text{pk},R}$ support previous studies that showed that respiration rates peak at higher temperatures than photosynthesis \citep{Huve2011, OSullivan2013}. While the thermal peak of photosynthesis generally matches mean daily temperatures \citep{Slatyev1978, Berry1980}, respiration rates can peak at temperatures above 50$^{\circ}$C \citep{Huve2011, Knight2002}. Our results show that this divergent response between $R$ and $P_\text{net}$ alone could potentially have implications for responses of both CUE and net carbon gain to increases in temperature \citep{Salvucci2004, Hozain2010}. However, it is important to note that mechanisms such as acclimation may alter the TPCs of photosynthesis and respiration, thus dampening the predicted responses of CUE and net carbon gain to increases in temperature \citep{Drake2016, Reich2016}. Yet, divergences in the responses of $R$ and $P_\text{net}$ to high temperatures need to be considered in ecosystem models to predict the impact that temperature changes will have in our planet \citep{OSullivan2013}. Indeed, results presented here lead to new opportunities for carbon cycle models to carry out comprehensive and comparative analyses between aquatic and terrestrial ecosystems.

%%% COMMENTED to make discussion shorter

%%While species in these models are usually parameterized according to a unique value for respiration and other for photosynthesis \citep{Smith2013}. As we have shown, the thermal sensitivity of these two traits are much closer and follow a distribution much wider than it has been suggested. Collaboration between experimentalists and modelers should be as closer as possible in order to get accurate predictions.

%% It could be argued that our interpretation might be affected by the use of the median, and that if the mean would be used, our conclusion might be different. But, even if means were used, the 0.65 value is within their 95\% confident intervals (CI) in the case of terrestrial species, and the slightly higher values predicted for aquatic species are probably due to the scarcity of data. Moreover, accordingly to \citet{Dell2011}, while dealing with processes involving single species, it seems more appropiate to use the median, as most individuals will have values closer to it.

%% \hspace{1em}Overall, both traits show systematic and substantial variation of activation energies, including a clear right skewness in the distribution that has already been shown when analysing the thermal sensitivity at the intraspecific level \citep{Dell2011}. Variation in $E$, which has already been reported by recent studies \citep{Irlich2009, Dell2011, Englund2011, Nilsson-Ortman2013} is likely to be the result of several factors, such as variation in the sampling of range location across studies, variation in range location or during model fitting \citep{Pawar2016}. In fact, such limitations have been the main cause of the reduction in the number of valid curves in our dataset. For instance, many of the curves do not cover the full thermal range of the species, some just cover the downward and some others the upward curve.  As $E$ is determined by the rate of exponential increase within the $''$physiological temperature range$''$ \citep{Pawar2016} (Fig. \ref{fig:tpcEx}), we are only interested in upward curves, but a minimum of data points is needed by the model to be fit. On the other hand, there are not many studies where both R and P responses to temperature are measured for the same species (we just found pair-wise data for about 25\% of the species in our compilation). We therefore encourage the collection of high-quality data for more sensitive tests of the activation energy of P and R.

%% \hspace{1em}It is worth to mention that across our study, we have been dealing with $''$net photosynthesis$''$ and not $''$gross photosynthesis$''$. This is because in all the studies we have been able to collect data, they measure photosynthesis as net assimilation. But because these measurements are not equivalent, the conclusions outlined here can not be directly applicable to gross photosynthesis. Gross photosynthesis is the sum of net photosynthesis, dark respiration and photorespiration. As the rate of net photosynthesis is usually higher than respiration, at least until the optimum photosyntesis is reached (Fig. \ref{fig:tpc}), we expect the response of the gross fixation of inorganic C to temperature to have a similar performance than net photosynthesis, but showing higher rates and a displacement in the peak towards higher temperatures.


\footnotesize{
\section*{\large{Methods}}

\subsection*{\normalsize{Data acquisition}}
\setlength{\parindent}{1em}
\hspace{1em}We compiled a database of thermal performance curves (TPC) of respiration and photosynthesis extracted from the literature. We searched for studies that measured the intraspecific temperature response of respiration and photosynthesis, primarily selecting studies under saturating conditions of CO$_2$ and light. We digitized data from tables, text or figures using DataThief\todo{MR: should we mention PlotDigitizer as well, which is the software I used while working for you?} \citep{Tummers2006} and when possible, we contacted authors to obtain raw data. We found more than 183 data sources, including journal articles, published reports, and books. This yielded a subset containing 1173 TPC's for respiration rate and net photosynthesis from both aquatic and terrestrial habitats that correspond to a total of 293 species (85 aquatic; 208 terrestrial). We primarily  selected studies where CO$_{2}$ and light were under saturated conditions.

%\subsection*{Unit Conversions}


\subsection*{\normalsize{TPC model}}
\setlength{\parindent}{1em}
\hspace{1em}The thermal responses were modelled using a modification of the Sharpe-Schoolfield equation \citep{Schoolfield1981},
\begin{equation}
  \label{eq:schoolfield_T_h}
b(T) = b_0 \cdot \dfrac
{
	e^{\dfrac{-E_\text{}}{k} \cdot \left (\dfrac{1}{T} - \dfrac{1}{T_\text{ref}} \right ) }
}
{
	1 + \dfrac{E}{E_\text{D} - E} \cdot e^{\dfrac{E_\text{D}}{k} \cdot \left (\dfrac{1}{T_\text{pk}} - \dfrac{1}{T} \right )}
}.
\end{equation}
Here, $b$ is the value of the metabolic trait at a given temperature $T$ (K). $E$ is the activation energy (eV), which controls the rise of the curve up to the peak, $T_\text{pk}$ is the temperature at which the trait is maximised, $E_{D}$ is the de-activation energy (eV), which indicates the rate at which the trait falls after the peak, and $k$ is the Boltzmann constant ($8.617 \cdot 10^{-5}$ eV $K^{-1}$). The normalisation constant $b_{0}$ is the value of the trait at a reference temperature $T_\text{ref}$.
%% As $b_{0}$ does not have a biological interpretation because of the absence of $T_\text{ref}$, we recalculate the value of the trait at a temperature of 10$^{\circ}$C.
%%\todonum{BGC: I didn't understand these last two b0 sentences. Why are we bothered with b0? We are not interpreting b0 are we? I will have nightmares with b0. SSB: We were at the beginning, but it is true that we are not using it anymore so I will comment them}

\subsection*{\normalsize{Data quality and parameters estimation}}
\setlength{\parindent}{1em}
\hspace{1em}For inclusion in our analysis, curves needed to have nonzero measurements at at least five different temperatures. We fit the Sharpe-Schoolfield model using non-linear least-squares estimation using the minpack.lm package \citep{Elzhov2013} in R \citep{RCoreTeam2015}. All fits with ${R^2} < 0.5$ were excluded.

Although metabolic rates are typically unimodal across their full thermal range, some studies focus on a narrower temperature range so curves can be limited to a rise or a fall in the trait. As we are interested in the sensitivity of the rises, we used activation energies for curves with at least three data points below $T_\text{pk}$. This yielded a total of 195 values of $E$ for terrestrial species (92 for net photosynthesis and 103 for respiration rate) and 71 values for aquatic species (49 for net photosynthesis and 42 for respiration rate).

To ensure that $T_\text{pk}$ was reliably estimated in the fit, we only used those fitted $T_\text{pk}$ values that were within the measured thermal range of their respective curves. After applying such restriction, we ended with a total of 148 terrestrial species (112 TPC's for net photosynthesis and 36 TPC's for respiration rate) and 76 aquatic species (48 TPC's for net photosynthesis and 28 TPC's for respiration rate).

\subsubsection*{Treatment of pseudoreplicates}
\setlength{\parindent}{1em}
\hspace{1em}The presence of pseudoreplicates was treated in order to avoid non-independence of data. We define pseudoreplicates as those responses sharing taxon and experimental conditions. For each pseudoreplicate group, we calculated the weighted averaged of the $E$ and $T_\text{pk}$ parameters across the individual responses in that group. We used the number of data points across the individual responses within each pseudoreplicate group as weights, so the response with more data points would contribute correspondingly more towards the parameter estimate. We also considered two alternative approaches. The first consisted in selecting responses within each pseudoreplicate group with the most frequent activation energy, and from those, selecting the curve with the highest $R^2$ (Fig.~S4). The second approach involved selecting the curve with the highest $R^2$ (Fig.~S5). Results were insensitive to the approach used, and are therefore robust (see Supplementary Information).

\subsection*{\normalsize{Analysis}}
\setlength{\parindent}{1em}
\hspace{1em}
We generated distributions of $E$ and $T_\text{pk}$ for net photosynthesis and respiration rate, for both terrestrial and aquatic species, and also for specific groups (C3-C4 for terrestrial plants and macrophytes-phytoplankton for aquatic organisms, see Fig. S1). We made comparisons between the distributions by estimating ordinary means and 95\%CI. A bootstrap approach \citep{Efron1994} was used to calculate medians and their respective 95\% CI with the package $asbio$ \citep{Aho2014}, selecting "BCa" as the CI interval method.

For species for which both respiration rate and net photosynthesis were measured within the same experiment, we used a non-parametric test (under the assumption that the distributions of $E$, particularly, are not normally distributed), the Wilcoxon signed-rank test, to establish whether $E$ and $T_\text{pk}$ were significantly different for net photosynthesis and respiration.

\subsection*{\normalsize{CUE and net flux responses}}
\setlength{\parindent}{1em}
\hspace{1em}
To estimate CUE and net flux (as net carbon gain),  we only used species for which both respiration rate and net photosynthesis were measured within the same experiment (within-species datastet). Both CUE and net carbon gain were calculated at temperatures expressed relative to each individual species' $T_{\text{pk},P_\text{net}}$ (i.e., for $T=T_{\text{pk},P_\text{net}} + x$, where $x$ are different temperatures ranging from -5 to +5$^\circ$C at 2.5$^\circ$C intervals). CUE was calculated as 1-$R/P_\text{gross}$, and net flux as $P_\text{net} - R$. Because the units of NP and R varied across studies, we normalised net flux to be within zero and one. Most experiments measure net photosynthesis; we assumed $P_\text{gross} = P_\text{net} + R$.
}

\bibliography{PR_Metanalysis}

\bibliographystyle{nature}

%---------------------------------------------
%       		 				FIGURES
%---------------------------------------------
\clearpage
\section*{Figures}


\begin{figure}[h]
\centerline{\includegraphics[scale=0.65]{../results/Plots/IntraspPlotsTerAq.pdf}}
\caption{{\bf Thermal performance curves (TPCs) of photosynthesis and respiration and the resultant responses of carbon use efficiency (CUE) and net flux to temperature.} Panels A \& B show examples of typical TPCs of net photosynthesis (blue), gross photosynthesis (green) and respiration (red). Here, $E$ is the slope of the curve, which represents the sensitivity of the metabolic rate to temperature, and $T_\text{pk}$ is the temperature at which the rate is maximised. OTR is the operational thermal range, which represents the range between the lowest theoretical nonlethal temperature and $T_\text{pk}$ \citep{Pawar2016}. Panel A shows a scenario where photosynthesis and respiration have different activation energies and $T_\text{pk}$. In B, both rates have the same activation energy but different $T_\text{pk}$. Panel C shows the TPC of one of the species in our dataset, \textit{Cenchrus ciliaris} (for which both responses where measured in the same experiment. All metabolic rates are shown in log-scale; the inset in panel C shows the same result in linear scale. Panels D,E,F show the resultant CUE (black solid line) and net carbon gain (black dashed line) calculated using the thermal responses in the respective panels above.}
\label{fig:tpcEx}
\end{figure}

\clearpage

\begin{figure}
\centerline{\includegraphics[scale = 0.65]{../results/Plots/AqTer_EaTpkPairHist.pdf}}
\caption{{\bf Global comparison of the $T_\text{pk}$ and $E$ distributions between $P_\text{net}$ vs. $R$ in terrestrial and aquatic ecosystems.\todo[inline]{MR: I feel that when you mention $T_\text{pk}$ and $E$ together you should just use the same order throughout the manuscript, istead of changing it every now and then}} Top panels show the distribution of activation energy for respiration (red) and net photosynthesis (blue) in aquatic (A) and terrestrial (B) ecosystems. Although no statistically significant difference was found in $E$ for $P_\text{net}$ vs. $R$ in both habitats, it is relevant \todo[inline]{MR: did you mean, `it is relevant to notice'?} the higher variance of $E$ shown in aquatic ecosystems. Dashed coloured lines show the median values of the respective traits (red for respiration and blue for net photosynthesis), whereas black dotted lines shows the 0.65 \todo[inline]{MR: should there be an `eV' here?} threshold value. Bottom panels show the $T_\text{pk}$ distribution for $R$ and $P_\text{net}$ in both ecosystems, aquatic (C) and terrestrial (D). Here $R$ typically peaks at higher temperatures than net photosynthesis. See Table S1 for detailed results and Figs. S2 \& S3 for an extended version of the distributions.
%% Median values, 95\% CIs, median bias ($z$, from the bootstrap approach) and number of species ($n$) for: A) R: 0.87 eV (0.65-1.17), $z$=0.05, $n$=42; NP: 0.70 eV (0.56-0.96), $z$=0.05, $n$=49; B) R: 0.64 eV (0.62-0.70), $z$=0.002, $n$=91; NP: 0.61 eV (0.53-0.61), $z$=0.03, $n$=103. Mean values and CIs for A) R: 0.92 eV (0.79-1.05), NP: 0.90 eV (0.71-1.08); B) R: 0.73 eV (0.64-0.81), NP 0.68 eV (0.59-0.78). Mean values, CIs and $n$ for: C) R: 39.18$^{\circ}$C (32.73-45.61), $n$=35; NP: 28.14$^{\circ}$C (25.09-31.19), $n$=49; D) R: 44.51$^{\circ}$C (37.30-51.72), $n$=31; NP 27.94$^{\circ}$C (26.43-29.44), $n$=114. Median values, CIs and $z$ for: C) R: 33.56$^{\circ}$C (29.95-36.49), $z$=-0.54; NP: 26.01$^{\circ}$C (25.03-30.71), $z$=-0.3; D) R: 38.73$^{\circ}$C (34.73-46.59), $z$=-0.18; NP 27.37$^{\circ}$C (25.63-29.07), $z$=0.002.
}
\label{fig:ehist}
\end{figure}


% TODO: Deal with caption below - stuff should go in Methods, not here, but make sure it is in Methods to begin with!
\clearpage

\begin{figure}
\centerline{\includegraphics[scale=0.65]{../results/Plots/AqTer_E_Tpk_Scatter.pdf}}
\caption{{\bf Within-species comparison of the $T_\text{pk}$ and $E$ distributions between $P_\text{net}$ vs. $R$ in terrestrial and aquatic ecosystems.} Top panels show a comparison of (A) activation energy and (B) $T_\text{pk}$ for both $R$ and $P_\text{net}$. Each data point corresponds to one species for which both rates were measured in the same experiment. Blue points show aquatic species and green points are terrestrial species. $R$ has a higher temperature sensitivity in about 73\% of cases (64\% in aquatic species \& 83\% in terrestrial species) and peaks at higher temperatures than $P_\text{net}$ in about 91\% of times (100\% in aquatic species \& 71\% in terrestrial species). Bottom panels show the boxplots of the differences in activation energy (C) between $R$ and $P_\text{net}$ in aquatic and terrestrial species. Panel (D) shows the differences in $T_\text{pk}$ between respiration rate and net photosynthesis in aquatic and terrestrial species. Notice that as occur \todo[inline]{MR:occur is very formal. How about `it happens'? Unless formal is what you want, in that case it's cool.} in the global comparison (Fig. \ref{fig:ehist}), aquatic organisms show a higher variance of $E$ than terrestrial \todo[inline]{MR: add `ones'?}. Because of the few number of $T_\text{pk}$ values for terrestrial organisms (n=7), in this subset it is not appreciable that $R$ peaks at higher temperature than $P_\text{net}$, as shown in Fig. \ref{fig:ehist}. However, when a more conservative subset is used (Fig. S6), the result appears to be strongly significant.}
\label{fig:scatter}
\end{figure}

\clearpage
\begin{figure}
\centerline{\includegraphics[scale=0.65]{../results/Plots/CUEDecrease.pdf}}
\caption{{\bf Effect of climatic warming on organismal Carbon Use Efficiency (CUE) and net flux}. Panel A shows CUE estimated -- from the within-species subset -- at different temperatures relative to each specie's $T_{\text{pk},P_\text{net}}$. Red points show the corresponding mean values at each temperature. Blue (respectively green) boxplots correspond to aquatic (respectively terrestrial) species. Panels B and C show how the differences in $T_\text{pk}$ between $R$ and $P_\text{net}$ (x-axis) influence the change in CUE with warming (y-axis). \todo[inline]{MR: It might just be me, but the following sentence is a bit dense.}Panel B shows the decline in CUE from 2.5$^{\circ}$C below  $T_{\text{pk},P_\text{net}}$ upto that temperature, while panel C shows the decline in CUE from $T_{\text{pk},P_\text{net}}$ to 2.5$^{\circ}$C higher than that $T_\text{pk}$.  Blue (respectively green) points and regression lines are for aquatic (respectively terrestrial) species. Both scenarios show decreasing trends in CUE with increasing $T_\text{pk}$ difference, but only those for terrestrial species are significant. As expected, the decline in CUE is steeper for warming beyond $T_{\text{pk},P_\text{net}}$ (Panel C) compared to warming upto $T_\text{pk}$. (Panel B). Similar to panel A, panel D shows net carbon gain (normalized to allow comparison of rates measured in different units) estimated at different temperatures relative to each species' $T_{\text{pk},P_\text{net}}$. For net carbon gain, no relation with the differences between $T_{\text{pk},P_\text{net}}$ and $T_{\text{pk},R}$ was found.}
\label{fig:CUE}
\end{figure}

%% \begin{tabular}{ l  l | c  c   c   c |  c  c   c   c  c }

%% \multirow{2}{*}{} & & \multicolumn{4}{c|}{\bf{Linear regression}} & \multicolumn{5}{c}{\bf{Paired t-test}} \\
%%  & & \textit{a} & \textit{b}  & \textit{r$^2$} & \textit{p} &  \textit{t} & \textit{df} & \textit{Mean differences} & \textit{p} &\textit{95\% C.I}  \\[0.5ex]
%% \hline
%% \multirow{2}{*}{\bf{E}} & Terrestrial & 0.354 & 0.281  & 0.085 & 0.09 & 2.421 & 23 & 0.164 & 0.024 & 0.024-0.304\\
%% & Aquatic & -0.443 & 1.373 &   0.026 & 0.2 & 2.484 & 27 & 0.044 & 0.806 & -0.319-0.407\\
%% \hline
%% \multirow{2}{*}{$\bf{T_{pk}}$} & Terrestrial & 0.766 & -1.161  & 0.476 & 8.07E-05 & 8.4935 & 24 & 10.05 & 1.076E-08 & 7.608-12.y492\\
%% & Aquatic & 0.703 & 2.724 &   0.52 & 9.049E-06 & 8.86 & 27 & 7.006 & 1.783E-09 & 5.384-8.629\\
%% \hline
%% \multirow{2}{*}{$\bf{B_{0}}$} & Terrestrial & -1.806E-01 & 8.312E-06  & -0.054 & 0.789 & -3.591 & 18 & -6.371E-06 & 0.002 & -1.01E-05 -2.643E-06\\
%% & Aquatic & 4.433 & 6.689E-06 &   0.581 &  0.001 & -3.465 & 12 & -4.921E-05 & 0.005 & -8.016E-05 -1.827E-055\\


\end{document}

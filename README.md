##########################################################################################################
**   Repository for the metanalysis of the temperature sensitivities of P&R in terrestrial plants **

*** CODE folder: Contains R scripts to fit TPC curves and for the meta-analysis of P&R data.

   - UnitConversionForP&RSubset.R: This file creates the subset of data from Biotraits, does the unit conversions and saves two files: PhotoRes.Data (including terrestrial and aquatic data) and TerData.Rdata (just terrestrial data)
   - TerAnalysis.R: Sofia's code for the meta-analysis of terrestrial P&R data. It creates the main file TerDataFinalPlants.Rdata
   - AquaticAnalysis.R: Sofia's code for the meta-analysis of aquatic P&R data. It creates the main file Aquatic.Rdata
   - RampingData.R: Comparison of Atkin's data (rapid acclimation) to "normal" acute responses.
   - flux.R: Bernardo's code to estimate net flux for individual species (It takes data from PairWiseTerrestrial and PairWiseAquatic).
   - OrganismTPC_GYD.R: Gab's code
   - Plots: Include all codes used to make the plots for the manuscript.
 
*** DATA folder: Includes data files to run using the scripts in the code folder.
   - Atkin: Atkin's data
   - AqPairData, AqPairDataRealPk, TerPairData and TerPairDataRealPk are calculated in Plots/PairWiseTerrestrial.R & Plots/PairWiseAquatic.R and stores the pairs of E and Tpk data of R and P for those species that were analysed within the same study. They also store the estimated CUE (from Plot/RatePredicts.R) and estimated NIR (from flux.R) at the Tpk of NP, and -5, -2.5, +.2.5, 5 (all of them calculated respect the Tpk(NP)). The RealPk version uses the observed Tpk's instead the ones estimated by the Schoolfield model.
   - TerData -> TerDataSatInf -> TerDataFinal -> TerDataFinalPlants.Rdata (latest version). It is the final subset of terrestrial data (P & R)  used for the analysis.
   - Aquatic.Rdata: Subset of aquatic data (P& R only) used for the analysis.
   - PhotoRes.Rdata: Subset from GlobalDataSet.Rdata, containing only TPC's of respiration rate and photosynthesis, once the units have been standardized (comes from UnitConversionForP%RSubset.R).
   - CO2LightSat.csv: Compilation of information about the conditions of CO2 and light at which the experiments were carried out.
   - PlantsC3C4.csv: Includes classification of the species in C3 or C4.

*** RESULTS folder: Meta-analysis' results

   - Plots: Contains plots for the main manuscript and SI.
   - nlsLM_v0.6AqNoTref: Individual plots (thermal response fits from the schoolfield model) for aquatic data.
   - nlsLM_v0.4NoTref: Individual plots (thermal response fits from the schoolfield model) for terrestrial data.

*** MANUSCRIPT folder: Meta-analysis' paper (latex and doc versions) and cover letter.

*** PAPERS folder: Useful bibliography


March 2017

*Please email Sofia Sal <s.sal-bregua@imperial.ac.uk> if you have 
questions.*